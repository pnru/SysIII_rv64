#include "sys/types.h"
#include "sys/param.h"
#include "sys/dir.h"
#include "sys/user.h"
#include "sys/proc.h"
#include "sys/vm.h"

pagetable_t kernel_pagetable;
pte_t *u_pte;

extern char etext[]; 	  // kernel.ld sets this to end of kernel code.
extern char trampoline[]; // trampoline.s

void kvmmap(pagetable_t kpgtbl, u64 va, u64 pa, u64 sz, int perm);
void *trampo;
extern int trapdisp();

/* Make a direct-map page table for the kernel. */
void kpt_init(upa)
	struct user *upa;
{
	pagetable_t	kpt = (pagetable_t) kalloc();

	/* map IO registers RW (uart, PLIC) */
	kvmmap(kpt, UART0,   UART0,   PGSIZE,   PTE_R | PTE_W);
	kvmmap(kpt, VIRTIO0, VIRTIO0, PGSIZE,   PTE_R | PTE_W);
	kvmmap(kpt, PLIC,    PLIC,    0x400000, PTE_R | PTE_W);
	kvmmap(kpt, 0x03000000LL,   0x03000000LL,  0x1000, PTE_R | PTE_W);
	kvmmap(kpt, 0x04020000LL,   0x04020000LL,  0x1000, PTE_R | PTE_W);

	
	/* map kernel text RX and rest RW  -- XXX page aligned kernel.bin */
	kvmmap(kpt, KERNBASE,   KERNBASE,   (u64)etext-KERNBASE-4095, PTE_R|PTE_X);
	kvmmap(kpt, (u64)etext, (u64)etext, PHYSTOP-(u64)etext,       PTE_R|PTE_W|PTE_X);
	
	/* map swap disk space XXX */
	kvmmap(kpt, PHYSTOP, PHYSTOP, (1LL<<22), PTE_R | PTE_W);  

	/* map the trap trampoline at top of RV39 space */
	trampo = kalloc();
	bcopy(trampoline, trampo, PGSIZE);
	((u64*)trampo)[511] = (u64)trapdisp;
	kvmmap(kpt, TRAMPOLINE, (u64)trampo, PGSIZE, PTE_R|PTE_X);
	
	/* map & clear the kernel u page */
	kvmmap(kpt, (u64)&u,    (u64)upa,    PGSIZE, PTE_R|PTE_W|PTE_V);
	memset(upa, 0, sizeof(struct user));

	kernel_pagetable = kpt;
}

pte_t *walk(pagetable_t pagetable, u64 va, int alloc);

/* Switch h/w page table register to the kernel's page table,
	* and enable paging.
	*/
void mmu_start(void)
{
	u_pte = walk(kernel_pagetable, (u64)&u, 0);
	if(!u_pte)
		panic("struct user not in kernel page table\n");

	w_satp(MAKE_SATP(kernel_pagetable));
	sfence_vma();
	adjust_sp();
}

/* Return the address of the PTE in page table pagetable
	* that corresponds to virtual address va.  If alloc!=0,
	* create any required page-table pages.
	*
	* The risc-v Sv39 scheme has three levels of page-table
	* pages. A page-table page contains 512 64-bit PTEs.
	* A 64-bit virtual address is split into five fields:
	*   39..63 -- must be zero.
	*   30..38 -- 9 bits of level-2 index.
	*   21..29 -- 9 bits of level-1 index.
	*   12..20 -- 9 bits of level-0 index.
	*    0..11 -- 12 bits of byte offset within the page.
	*/
pte_t *walk(pagetable_t pagetable, u64 va, int alloc)
{	
	if(va >= MAXVA)
		panic("walk va");
		
	if(pagetable==0)
		panic("walk pt");

	for(int level = 2; level > 0; level--) {
		pte_t *pte = &pagetable[PX(level, va)];
		if(*pte & PTE_V) {
			pagetable = (pagetable_t)PTE2PA(*pte);
		} else {
			if(!alloc || (pagetable = kalloc()) == 0)
				return 0;
			memset(pagetable, 0, PGSIZE);
			*pte = PA2PTE(pagetable) | PTE_V;
		}
	}
	return &pagetable[PX(0, va)];
}

/* Look up a virtual address, return the physical address,
	* or 0 if not mapped.  Can only be used to look up user pages.
	*/
u64 walkaddr(pagetable_t pagetable, u64 va)
{
	pte_t *pte;
	u64 pa;

	if(va >= MAXVA)
		return 0;

	pte = walk(pagetable, va, 0);
	if(pte == 0)
		return 0;
	if((*pte & PTE_V) == 0)
		return 0;
	//if((*pte & PTE_U) == 0)
	//	return 0;
	pa = PTE2PA(*pte);
	return pa;
}

void testwalk(pagetable_t pagetable, u64 va)
{
	pte_t *pte;
	u64 pa;
	
	if(va >= MAXVA)
		return;
	
	pte = walk(pagetable, va, 0);
	pa = PTE2PA(*pte);
	printf("  va = %p\n",  va );
	printf("&pte = %p\n",  pte);
	printf(" pte = %p\n", *pte);
	printf("  pa = %p\n",  pa );
}

/* Add a mapping to the kernel page table. Only used when booting.
	* Does not flush TLB or enable paging.
	*/
void kvmmap(pagetable_t kpgtbl, u64 va, u64 pa, u64 sz, int perm)
{
	if(mappages(kpgtbl, va, sz, pa, perm) != 0)
		panic("kvmmap");
}

/* Create PTEs for virtual addresses starting at va that refer to
	* physical addresses starting at pa. va and size might not
	* be page-aligned. Returns 0 on success, -1 if walk() couldn't
	* allocate a needed page-table page.
	*/
int mappages(pagetable_t pt, u64 va, u64 size, u64 pa, int perm)
{
	u64 a, last;
	pte_t *pte;

	a = PGROUNDDOWN(va);
	last = PGROUNDDOWN(va + size - 1);
//printf("f=%p,t=%p,f=%p\n", a, last, pa);
	for(;;){
		if((pte = walk(pt, a, 1)) == 0) {
//printf("map fail at %p\n", a);
			return -1;
		}
		if((*pte & PTE_V)) {
			printf("va=%p, pid=%d\n", a, u.u_procp->p_pid);
			panic("remap");
		}
		*pte = PA2PTE(pa) | perm | PTE_V | PTE_A | PTE_D ;
		if(a == last)
			break;
		a  += PGSIZE;
		pa += PGSIZE;
	}
	return 0;
}

uvmlist(pagetable_t pagetable, u64 va, u64 npages)
{
	u64 a, n;
	pte_t *pte;
	
	if((va % PGSIZE) != 0)
		panic("uvmlist: not aligned");
	
	for (a = va; a < va + npages*PGSIZE; a += PGSIZE) {
		if ((pte = walk(pagetable, a, 0)) == 0)
			n = 0;
		else
			n = *pte;
		printf("pte[%p] = %p\n", a, n);
	}
}

// Remove npages of mappings starting from va. va must be
// page-aligned. The mappings must exist.
// Optionally free the physical memory.
void uvmunmap(pagetable_t pagetable, u64 va, u64 npages, int do_free)
{
	u64 a;
	pte_t *pte;

	if((va % PGSIZE) != 0)
		panic("uvmunmap: not aligned");

	for(a = va; a < va + npages*PGSIZE; a += PGSIZE){
		if((pte = walk(pagetable, a, 0)) == 0)
			panic("uvmunmap: walk");
		if((*pte & PTE_V) == 0)
			continue; //panic("uvmunmap: not mapped");
		if(PTE_FLAGS(*pte) == PTE_V)
			panic("uvmunmap: not a leaf");
		if(do_free){
			if (*pte & PTE_L)
				panic("freeing linked page");
			u64 pa = PTE2PA(*pte);
			kfree((void*)pa);
		}
		*pte = 0;
	}
}

/* Allocate PTEs and physical memory to grow process from oldsz to
 * newsz, which need not be page aligned.  Returns new size or 0 on error.
 */
u64 uvmalloc(pagetable_t pt, u64 va_low, u64 va_high, int perm)
{
	char *mem;
	u64 va;
//printf("lo=%p, hi=%p\n", va_low, va_high);
	if(va_high < va_low)
		return va_low;

	va_low = PGROUNDDOWN(va_low);
	for(va = va_low; va < va_high; va += PGSIZE){
		mem = kalloc();
		if(mem == 0){
//printf("X");
			uvmdealloc(pt, va, va_low, 1);
			return 0;
		}
		if(mappages(pt, va, PGSIZE, (u64)mem, perm|PTE_U) != 0){
			kfree(mem);
			uvmdealloc(pt, va, va_low, 1);
			return 0;
		}
	}
	return va_high;
}

void uvmflip(pagetable_t pt, unsigned hi)
{
	pte_t	 *pte;
	unsigned i;

	for(i = 0; i < hi; i++){
		if((pte = walk(pt, ((u64)i)<<12, 0)) == 0)
			panic("uvmflip: pte should exist");
		if((*pte & PTE_V) == 0) {
printf("i=%x, hi=%x\n", i, hi);
			panic("uvmflip: page not present");
		}
		*pte ^= PTE_L;
	}
}

// Deallocate user pages to bring the process size from oldsz to
// newsz.  oldsz and newsz need not be page-aligned, nor does newsz
// need to be less than oldsz.  oldsz can be larger than the actual
// process size.  Returns the new process size.
u64
uvmdealloc(pagetable_t pagetable, u64 oldsz, u64 newsz, int do_free)
{
	if(newsz >= oldsz)
		return oldsz;
//printf("in uvmdealloc pt=%p, old=%p, new=%p, free=%x\n", pagetable, oldsz, newsz, do_free);
	if(PGROUNDUP(newsz) < PGROUNDUP(oldsz)){
		int npages = (PGROUNDUP(oldsz) - PGROUNDUP(newsz)) / PGSIZE;
		uvmunmap(pagetable, PGROUNDUP(newsz), npages, do_free);
	}

	return newsz;
}

/* Recursively free page-table pages and all leaf pages. A pte with
	* the valid bit set, but none of RWX, links to a lower level directory
	* page.
	*/
void uvmzap(pagetable_t pt)
{
	pte_t	pte;
	
//	printf("---\n");
	for(int i = 0; i < 512; i++){
		pte = pt[i];
		if((pte & PTE_V) && (pte & (PTE_R|PTE_W|PTE_X)) == 0){
			uvmzap((pagetable_t)PTE2PA(pte));
		} else if((pte & PTE_V) && !(pte & PTE_L)){
//			printf("pte[%d]=%p\n", i, pte);
			if (isfree((void*)PTE2PA(pte))) {
				panic("data double free\n");
			}
			kfree((void*)PTE2PA(pte));
		}
		pt[i] = 0;
	}
	if (isfree(pt))
		panic("pt double free\n");
	kfree((void*)pt);
}

// Recursively free page-table pages.
// All leaf mappings must already have been removed.
void
freewalk(pagetable_t pagetable)
{
	// there are 2^9 = 512 PTEs in a page table.
	for(int i = 0; i < 512; i++){
		pte_t pte = pagetable[i];
		if((pte & PTE_V) && (pte & (PTE_R|PTE_W|PTE_X)) == 0){
			// this PTE points to a lower-level page table.
			u64 child = PTE2PA(pte);
			freewalk((pagetable_t)child);
			pagetable[i] = 0;
		} else if(pte & PTE_V){
			panic("freewalk: leaf");
		}
	}
	kfree((void*)pagetable);
}

// Free user memory pages,
// then free page-table pages.
void
uvmfree(pagetable_t pagetable, u64 sz)
{
	if(sz > 0)
		uvmunmap(pagetable, 0, PGROUNDUP(sz)/PGSIZE, 1);
	freewalk(pagetable);
}

/* Given a parent process's page table, copy its memory into a child's page
	* table. Copies both the page table and the physical memory.
	* Returns 0 on success, -1 on failure; frees any allocated pages on failure.
	*/
int uvmcopy(pagetable_t old, pagetable_t new, u64 va, u64 sz, int ref)
{
	pte_t *pte;
	u64 pa, i;
	int flags;
	char *mem;

	va = PGROUNDDOWN(va);
	for(i = 0; i < sz; i += PGSIZE){
//printf("i=%p, sz=%p\n", i, sz);
//printf("va=%p\n", va+i);
//printf("old=%p, new=%p\n", old, new);
		if((pte = walk(old, va+i, 0)) == 0)
			panic("uvmcopy: pte should exist");
		if((*pte & PTE_V) == 0) {
printf("i=%p, sz=%p, pte=%p\n", i, sz, *pte);
			panic("uvmcopy: page not present");
		}
		pa = PTE2PA(*pte);
		flags = PTE_FLAGS(*pte);
		if (ref) {
			mem = (char*)PTE2PA(*pte);
			flags |= PTE_L;
		}
		else {
			if ((mem = kalloc()) == 0)
				goto err;
			bcopy((void*)pa, mem, PGSIZE);
		}
		if(mappages(new, va+i, PGSIZE, (u64)mem, flags) != 0){
			if (!ref) kfree(mem);
			goto err;
		}
	}
	return 0;

err:
	uvmunmap(new, va, i / PGSIZE, ref?0:1);
	return -1;
}

// mark a PTE invalid for user access.
// used by exec for the user stack guard page.
void
uvmclear(pagetable_t pagetable, u64 va)
{
		pte_t *pte;
		
		pte = walk(pagetable, va, 0);
		if(pte == 0)
				panic("uvmclear");
		*pte &= ~PTE_U;
}

typedef u64 word;

#include <sys/buf.h>

int useracc(caddr_t addr, word count, int flag)
{
	pte_t *pte;
	int    n, test = (flag & B_READ) ? PTE_R : PTE_W;
	
	while (count > 0) {
		pte = walk(u.u_procp->p_pt, (word)addr, 0);
		if ((*pte & test) == 0)
			return 0;
		n = (count > PGSIZE) ? PGSIZE : count;
		count -= n;
		addr  += n;
	}
	pte = walk(u.u_procp->p_pt, (word)addr, 0);
	return (*pte & test) ? 1 : 0;
}

/* Data movement between kernel and user space
 */

int fubyte(void *addr)
{
	word pa, va = (word)addr;
	int i;
	pa = (word)walkaddr(u.u_procp->p_pt, va);
	if(pa == 0)
		return (-1);
	pa += PGOFFS(va);
	i = *((unsigned char  *)pa);
	return i;
}

int subyte(void *addr, unsigned char val)
{
	word pa, va = (word)addr;

	pa = (word)walkaddr(u.u_procp->p_pt, va);
	if(pa == 0)
		return (-1);
	pa += PGOFFS(va);
	*((char  *)pa) = val;
	return 0;
}

word fuword(void *addr)
{
	int sz = sizeof(word);
	word val = 0, pa, va = (word)addr + sz;
	int i, c;

	for (i=0; i<sz; i++) {
		if ((c = fubyte((void*)--va)) < 0 )
			return -1;
		val = (val<<8) | c;
	}
	return val;
}

int suword(void *addr, word val)
{
	int sz = sizeof(word);
	word pa, va = (word)addr;
	int i;

	for (i=0; i<sz; i++) {
		if (subyte((void*)va++, val&0xff) < 0 )
			return -1;
		val >>= 8;
	}
	return 0;
}

// Copy from kernel to user.
// Copy len bytes from src to virtual address dstva in a given page table.
// Return 0 on success, -1 on error.
int
copyout(caddr_t src, word dstva, u32 len)
{
	word n, pa;
	pte_t *pt = u.u_procp->p_pt;

//printf("OUT: %p, %p, %p\n", src, dstva, len);
	while(len > 0){
		pa = walkaddr(pt, dstva);
		if(pa == 0)
			return -1;
		n = PGSIZE - PGOFFS(dstva);
		if(n > len)
			n = len;
		bcopy(src, (void *)(pa + PGOFFS(dstva)), n);
//printf("OUT: %p, %p, %p\n", src, pa0 + (dstva - va0), n);
		len -= n;
		src += n;
		dstva = PGROUNDDOWN(dstva) + PGSIZE;
	}
	return 0;
}

// Copy from user to kernel.
// Copy len bytes to dst from virtual address srcva in a given page table.
// Return 0 on success, -1 on error.
int
copyin(word srcva, caddr_t dst, u32 len)
{
	word n, pa;
	pte_t *pt = u.u_procp->p_pt;

	while(len > 0){
		pa = walkaddr(pt, srcva);
		if(pa == 0)
			return -1;
		n = PGSIZE - PGOFFS(srcva);
		if(n > len)
			n = len;
		bcopy((void *)(pa + PGOFFS(srcva)), dst, n);
		len -= n;
		dst += n;
		srcva = PGROUNDDOWN(srcva) + PGSIZE;
	}
	return 0;
}

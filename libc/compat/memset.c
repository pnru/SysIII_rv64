void *memset(s, ucharfill, size)
char *s;
int ucharfill;
int  size;
{
  register char *scan;
  register int n;
  register int uc = ucharfill & 0xff;

  scan = (char *) s;
  for (n = size; n > 0; n--) *scan++ = uc;

  return(s);
}


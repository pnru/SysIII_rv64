	//
	// interrupts and exceptions come here
	//
	// save CPU state, call do_trap(), restore, return.
	// this code is copied to a page located at virtual
	// address TRAMPOLINE, so it must be written to be
	// position independent.
	//
	
#define SRET		WORD $0x10200073
#define SFENCE_VMA	WORD $0x12000073
	
#define SPP		$0x100		// previous mode bit, 1=Supervisor, 0=User

	// this code is mapped at the same virtual address
	// (TRAMPOLINE) in user and kernel space so that
	// it continues to work when the CPU switches page
	// tables.

TEXT trampoline(SB),0,$-8

exception:
	// Save R6 to sscratch, and test the previous
	// mode. If we came from kernel mode (i.e. there is
	// an interrupt), just continue on the same stack.
	//
	CSRRW	CSR(0x0140), R6, R6
	MOV	CSR(0x0100), R6
	AND	SPP, R6			// previous mode supervisor?
	BEQ	R6, R0, usertrap	// no -> switch page table
	CSRRW	CSR(0x0140), R6, R6

	// make room to save registers.
	SUB	$256,R2,R2

	// kernel trap, continue on same page table & same stack
	//
	MOV R1,    0(R2)
	MOV R2,    8(R2)
	MOV R3,   16(R2)
	MOV R4,   24(R2)
	MOV R5,   32(R2)
	MOV R6,   40(R2)
	MOV R7,   48(R2)
	MOV R8,   56(R2)
	MOV R9,   64(R2)
	MOV R10,  72(R2)
	MOV R11,  80(R2)
	MOV R12,  88(R2)
	MOV R13,  96(R2)
	MOV R14, 104(R2)
	MOV R15, 112(R2)
/*	MOV R16, 120(R2)
	MOV R17, 128(R2)
	MOV R18, 136(R2)
	MOV R19, 144(R2)
	MOV R20, 152(R2)
	MOV R21, 160(R2)
	MOV R22, 168(R2)
	MOV R23, 176(R2)
	MOV R24, 184(R2)
	MOV R25, 192(R2)
	MOV R26, 200(R2)
	MOV R27, 208(R2)
	MOV R28, 216(R2)
	MOV R29, 224(R2)
	MOV R30, 232(R2)
	MOV R31, 240(R2)
*/

	// call the C trap dispatcher in trap.c
	// structured to be position independent:
	// the last long word of the trampoline is
	// filled with a pointer to the trap dispatcher.
	//
	MOV	$0x3ffffff, R1
	SLL	$12, R1
	MOV	4088(R1), R1
	JAL	,0(R1)

	// restore registers.
	MOV   0(R2), R1
	MOV   8(R2), R2
	MOV  16(R2), R3
	MOV  24(R2), R4
	MOV  32(R2), R5
	MOV  40(R2), R6
	MOV  48(R2), R7
	MOV  56(R2), R8
	MOV  64(R2), R9
	MOV  72(R2), R10
	MOV  80(R2), R11
	MOV  88(R2), R12
	MOV  96(R2), R13
	MOV 104(R2), R14
	MOV 112(R2), R15
/*	MOV 120(R2), R16
	MOV 128(R2), R17
	MOV 136(R2), R18
	MOV 144(R2), R19
	MOV 152(R2), R20
	MOV 160(R2), R21
	MOV 168(R2), R22
	MOV 176(R2), R23
	MOV 184(R2), R24
	MOV 192(R2), R25
	MOV 200(R2), R26
	MOV 208(R2), R27
	MOV 216(R2), R28
	MOV 224(R2), R29
	MOV 232(R2), R30
	MOV 240(R2), R31
*/
	ADD	$256,R2,R2

	// return to whatever we were doing in the kernel.
	SRET

usertrap:
	// code to switch between user and kernel space.
	//
	// CPU is in supervisor mode, but with a
	// user page table. TRAMPOLINE is mapped in both
	// kernel and user space.

	MOV	$0x3fffffd, R6
	SLL	$12, R6		// R6 = 0x3fffffd000 = u page

        // save the user registers in u->u_pcb.Rx
        MOV R1,   40(R6)
        MOV R2,   48(R6)
        MOV R3,   56(R6)
        MOV R4,   64(R6)
        MOV R5,   72(R6)
        MOV R7,   88(R6)
	MOV R8,   96(R6)
	MOV R9,  104(R6)
	MOV R10, 112(R6)
        MOV R11, 120(R6)
        MOV R12, 128(R6)
        MOV R13, 136(R6)
        MOV R14, 144(R6)
        MOV R15, 152(R6)
        MOV R16, 160(R6)
        MOV R17, 168(R6)
        MOV R18, 176(R6)
        MOV R19, 184(R6)
        MOV R20, 192(R6)
        MOV R21, 200(R6)
        MOV R22, 208(R6)
        MOV R23, 216(R6)
        MOV R24, 224(R6)
        MOV R25, 232(R6)
        MOV R26, 240(R6)
        MOV R27, 248(R6)
        MOV R28, 256(R6)
        MOV R29, 264(R6)
        MOV R30, 272(R6)
        MOV R31, 280(R6)

	// save the user R6 in u->pcb.R6
        MOV	CSR(0x0140),R5
	MOV	R5,80(R6)

        // restore kernel stack pointer from u->u_pcb.kernel_sp
        MOV	8(R6),R2

        // make tp hold the current hartid, from u->u_pcb.kernel_hartid
	MOV	32(R6),R18
	
	// make gp hold the kernel global pointer (SB)
	MOV	288(R6),R3

        // load the address of usertrap(), u->u_pcb.kernel_trap
	MOV 	16(R6),R5

        // restore kernel page table from u->u_pcb.kernel_satp
	MOV	0(R6),R7
	MOV	R7,CSR(0x0180)	// satp
	SFENCE_VMA

        // jump to usertrap(), which does not return
        JMP	0(R5)

TEXT userret(SB),0,$0
        // "userret(&u, uptbl)"
        // switch from kernel to user.
        // R8:		"u->"
        // 16(SP):	user page table, for satp.
	
        // switch to the user page table.
	MOV	16(R2),R9
	MOV	R9,CSR(0x0180)	// pagetable -> satp
	SFENCE_VMA

        // put the saved user R8 in sscratch, so we
        // can swap it with our R8 (u->) in the last step.
        MOV	96(R8), R9
	MOV	R9, CSR(0x0140)

        // restore all but R8 from u->u_pcb.Rx
	MOV  40(R8), R1
        MOV  48(R8), R2
        MOV  56(R8), R3
        MOV  64(R8), R4
        MOV  72(R8), R5
        MOV  80(R8), R6
        MOV  88(R8), R7
        MOV 104(R8), R9
	MOV 112(R8), R10
        MOV 120(R8), R11
        MOV 128(R8), R12
        MOV 136(R8), R13
        MOV 144(R8), R14
        MOV 152(R8), R15
        MOV 160(R8), R16
        MOV 168(R8), R17
        MOV 176(R8), R18
        MOV 184(R8), R19
        MOV 192(R8), R20
        MOV 200(R8), R21
        MOV 208(R8), R22
        MOV 216(R8), R23
        MOV 224(R8), R24
        MOV 232(R8), R25
        MOV 240(R8), R26
        MOV 248(R8), R27
        MOV 256(R8), R28
        MOV 264(R8), R29
        MOV 272(R8), R30
        MOV 280(R8), R31

	// restore user R8
	CSRRW	CSR(0x0140), R8, R8
	
        // return to user mode and user pc.
        // userjmp() set up sstatus and sepc.
        SRET

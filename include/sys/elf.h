/* Format of an ELF executable file header */

#define ELF_MAGIC 0x464c457fu  // "\x7FELF" in little endian

#pragma pack on

/* File header */
struct elfhdr {
	u32	magic;
	u8	elf[12];
	u16	type;
	u16	machine;
	u32	version;
	u64	entry;
	u64	phoff;
	u64	shoff;
	u32	flags;
	u16	ehsize;
	u16	phentsize;
	u16	phnum;
	u16	shentsize;
	u16	shnum;
	u16	shstrndx;
};

/* Program section header */
struct proghdr {
	u32	p_type;
	u32	p_flags;
	u64	p_off;
	u64	p_vaddr;
	u64	p_paddr;
	u64	p_filesz;
	u64	p_memsz;
	u64	p_align;
};

#pragma pack off

/* Values for Proghdr type */
#define ELF_PROG_LOAD           1

/* Flag bits for Proghdr flags */
#define ELF_X	1
#define ELF_W	2
#define ELF_R	4

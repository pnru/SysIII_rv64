
/*
   process control block
 */

typedef unsigned long long u64;

struct pcb {
	/*   0 */ u64 kernel_satp;	// kernel page table
	/*   8 */ u64 kernel_sp;	// top of process's kernel stack
	/*  16 */ u64 kernel_addr;	// usertrap()
	/*  24 */ u64 epc;		// saved user program counter
	/*  32 */ u64 kernel_hartid;	// saved kernel tp
	/*  40 */ u64 R1;
	/*  48 */ u64 R2;
	/*  56 */ u64 R3;
	/*  64 */ u64 R4;
	/*  72 */ u64 R5;
	/*  80 */ u64 R6;
	/*  88 */ u64 R7;
	/*  96 */ u64 R8;
	/* 104 */ u64 R9;
	/* 112 */ u64 R10;
	/* 120 */ u64 R11;
	/* 128 */ u64 R12;
	/* 136 */ u64 R13;
	/* 144 */ u64 R14;
	/* 152 */ u64 R15;
	/* 160 */ u64 R16;
	/* 168 */ u64 R17;
	/* 176 */ u64 R18;
	/* 184 */ u64 R19;
	/* 192 */ u64 R20;
	/* 200 */ u64 R21;
	/* 208 */ u64 R22;
	/* 216 */ u64 R23;
	/* 224 */ u64 R24;
	/* 232 */ u64 R25;
	/* 240 */ u64 R26;
	/* 248 */ u64 R27;
	/* 256 */ u64 R28;
	/* 264 */ u64 R29;
	/* 272 */ u64 R30;
	/* 280 */ u64 R31;
	/* 288 */ u64 kernel_gp;	// kernel global pointer
};


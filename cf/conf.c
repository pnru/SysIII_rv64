/*
 *  Configuration information
 */


#define	RAMDSK_0 1
#define	VIRTIO_0 1
#define	TRACE_0 1
#define	UART_0 1
#define	MEMORY_0 1
#define	TTY_0 1
#define	ERRLOG_0 1

#define	NBUF	48
#define	NINODE	125
#define	NFILE	125
#define	NMOUNT	8
#define	CMAPSIZ	75
#define	SMAPSIZ	75
#define	NCALL	50
#define	NPROC	65
#define	NTEXT	40
#define	NCLIST	150
#define	NSABUF	8
#define	POWER	0
#define	MAXUP	25
#define	NHBUF	64

#include	"sys/param.h"
#include	"sys/types.h"
#include	"sys/io.h"
#include	"sys/iobuf.h"
#include	"sys/space.h"
#include	"sys/conf.h"

extern nodev(), nulldev();
extern rdopen(), rdclose(), rdread(), rdwrite(), rdstrategy();
extern struct iobuf rdtab;
extern vtopen(), vtclose(), vtread(), vtwrite(), vtinit(), vtstrategy();
extern struct iobuf vttab;
extern tropen(), trclose(), trread(), trioctl();
extern uaopen(), uaclose(), uaread(), uawrite(), uaioctl();
extern mmread(), mmwrite();
extern syopen(), syread(), sywrite(), syioctl();
extern erropen(), errclose(), errread();

struct bdevsw bdevsw[] = {
/* 0*/	nodev, 	nodev, 	nodev, 	0, 
/* 1*/	rdopen,	rdclose,	rdstrategy,	&rdtab,
/* 2*/	vtopen,	vtclose,	vtstrategy,	&vttab,
};

struct cdevsw cdevsw[] = {
/* 0*/	uaopen,	uaclose,	uaread,	uawrite,	uaioctl,
/* 1*/	nodev, 	nodev, 	nodev, 	nodev, 	nodev,
/* 2*/	nulldev,	nulldev,	mmread,	mmwrite,	nodev, 
/* 3*/	nodev, 	nodev, 	nodev, 	nodev, 	nodev,
/* 4*/	rdopen,	rdclose,	rdread,	rdwrite,	nodev, 
/* 5*/	vtopen,	vtclose,	vtread,	vtwrite,	nodev, 
/* 6*/	nodev, 	nodev, 	nodev, 	nodev, 	nodev,
/* 7*/	tropen,	trclose,	trread,	nodev, 	trioctl,
/* 8*/	syopen,	nulldev,	syread,	sywrite,	syioctl,
/* 9*/	erropen,	errclose,	errread,	nodev, 	nodev, 
};

int	bdevcnt = 3;
int	cdevcnt = 10;

dev_t	rootdev = makedev(2, 0);
dev_t	pipedev = makedev(2, 0);
dev_t	dumpdev = makedev(2, 0);
dev_t	swapdev = makedev(1, 0);
daddr_t	swplo = 0;
int	nswap = 15000;


int	rd_cnt = 2;
int	vt_cnt = 1;
int	ua_cnt = 1;


int	tr_cnt = 1;



int	(*dev_init[])() = 
{
	&vtinit,
	(int (*)())0
};

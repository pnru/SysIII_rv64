
#include <errno.h>

rmdir(path)
char *path;
{
	register int child, pid;
	int status;
	extern errno;

	switch (child = fork()) {
	case -1:
		return(-1);
	case 0:
		/* so that we don't get output on errors */
		close(1);
		close(2);

		execl("/bin/rmdir", "rmdir", path, 0);
		exit(2);
	default:
		while ((pid = wait(&status)) > 0)
			if (pid == child)
				break;
		if (pid < 0 || status == (2<<8)) {
			errno = ENOEXEC;
			return(-1);
		}
		if (status) {
			errno = EACCES;
			return(-1);
		}
		return(0);
	}
}


/*
 * Disk driver stub
 */

#include "sys/types.h"
#include "sys/param.h"
#include "sys/dir.h"
#include "sys/user.h"
#include "sys/buf.h"
#include "sys/elog.h"
#include "sys/iobuf.h"
#include "sys/systm.h"
#include "sys/vm.h"

#include "boot.h"

struct	iobuf	bttab;

btopen(int dev)
{
	;
}

btclose(int dev)
{
}

btstrategy(struct buf *bp)
{
	if ( (bp->b_blkno * BSIZE >= fs_img_len) || (bp->b_blkno < 0) )
	{
		bp->b_flags |= B_ERROR;
		iodone(bp);
		return;
	}

	bp->av_forw = 0;
	/* link buffer to end of service queue */
	spl7();
	if (bttab.b_actf == 0)
		bttab.b_actf = bp;
	else
		bttab.b_actl->av_forw = bp;
	bttab.b_actl = bp;
	/* fdtab.b_active is always 0 for now */
	if (bttab.b_active == 0)
		btstart();
	spl0();
}

int bkread(int block, char* to, int len)
{
	u8 *f;
//printf("DSK RD %d, %d, %p\n", block, len, to);
	f = fs_img + block * BSIZE;
			     
	while(len--) *to++ = *f++;
	return 0;
}

int bkwrite(int block, char* from, int len)
{
	u8 *t;
//printf("DSK WR %d, %d, %p\n", block, len, from);
	t = fs_img + block * BSIZE;
			     
	while(len--) *t++ = *from++;
	return 0;
}

btstart(void)
{
	register struct buf *bp;
	int rc;

	/* Unlink immediately; there is always 0 or 1 request in the queue */
	if ((bp = bttab.b_actf) == 0)
		return;
	bttab.b_actf = bp->av_forw;
	
	/* Read or write the buffer to the CF Card. Normal I/O to a kernel
	 * disk buffer is easy, direct I/O to user space (swapping, "raw" disk
	 * access) requires juggling the page map.
	 */
	if(bp->b_flags & B_PHYS) {
		printf("direct I/O not yet implemented\n");
	} else {
		if(bp->b_flags & B_READ)
		  rc = bkread(bp->b_blkno, (char*)paddr(bp),  bp->b_bcount);
		else
		  rc = bkwrite(bp->b_blkno, (char*)paddr(bp), bp->b_bcount);
	}
	if(rc) 
		bp->b_flags |= B_ERROR;
	iodone(bp);
}

btintr(void)
{
}

struct	buf	btbuf;

btread(int dev)
{
		physio(btstrategy, &btbuf, dev, B_READ);
}

btwrite(int dev)
{
		physio(btstrategy, &btbuf, dev, B_WRITE);
}


#include "sys/types.h"
#include "sys/param.h"
#include "sys/dir.h"
#include "sys/user.h"
#include "sys/proc.h"
#include "sys/vm.h"

void w_sstatus(u64 x);
u64 r_sstatus();

//#define SSTATUS_SIE (1L << 1)

int locked()
{
	u64 x = r_sstatus();
	return !(x & SSTATUS_SIE);
}

// enable device interrupts
int spl0()
{
	u64 x = r_sstatus();
	w_sstatus(x | SSTATUS_SIE);
//printf("int on\n");
	return (x & SSTATUS_SIE) ? 0 : 7;
}

// disable device interrupts
int spl7()
{
	u64 x = r_sstatus();
	w_sstatus(x & ~SSTATUS_SIE);
//printf("int off\n");
	return (x & SSTATUS_SIE) ? 0 : 7;
}

int splx(int s)
{
	u64 x = r_sstatus();
	if(s)	{w_sstatus(x & ~SSTATUS_SIE); /*printf("intx off\n");*/ }
	else	{w_sstatus(x |  SSTATUS_SIE); /*printf("intx on\n");*/ }
	return (x & SSTATUS_SIE) ? 0 : 7;
}

int spl6()
{
	return spl7();
}

int spl5()
{
	return spl7();
}

int min(int a, int b)
{
	return (a<b ? a : b);
}

int max(int a, int b)
{
	return (a>b ? a : b);
}





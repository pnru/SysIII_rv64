#include "sys/param.h"
#include "sys/dir.h"
#include "sys/user.h"
#include "sys/proc.h"
#include "sys/text.h"
#include "sys/systm.h"
#include "sys/sysinfo.h"
#include "sys/map.h"
#include "sys/file.h"
#include "sys/inode.h"
#include "sys/buf.h"
#include "sys/var.h"
#include "sys/page.h"
#include "sys/vm.h"

int save(void* buf);
void resume(void* newu, void* buf);

/*
 * Give up the processor till a wakeup occurs
 * on chan, at which time the process
 * enters the scheduling queue at priority pri.
 * The most important effect of pri is that when
 * pri<=PZERO a signal cannot disturb the sleep;
 * if pri>PZERO signals will be processed.
 * Callers of this routine must be prepared for
 * premature return, and check that the reason for
 * sleeping has gone away.
 */
#define	TZERO	6
sleep(chan, pri)
caddr_t chan;
{
	register struct proc *rp;
	register s;

	rp = u.u_procp;
	s = spl6();
	rp->p_stat = SSLEEP;
	rp->p_wchan = chan;
	rp->p_pri = pri;
	if (rp->p_time > TZERO)
		rp->p_time = TZERO;
	if(pri > PZERO) {
		if(issig()) {
			rp->p_wchan = 0;
			rp->p_stat = SRUN;
			spl0();
			goto psig;
		}
		spl0();
		if(runin != 0) {
			runin = 0;
			wakeup((caddr_t)&runin);
		}
		swtch();
		if(issig())
			goto psig;
	} else {
		spl0();
		swtch();
	}
	splx(s);
	return;

	/*
	 * If priority was low (>PZERO) and
	 * there has been a signal,
	 * execute non-local goto to
	 * the qsav location.
	 * (see trap1/trap.c)
	 */
psig:
	longjmp(u.u_qsav);
}

/*
 * Wake up all processes sleeping on chan.
 */
wakeup(chan)
register caddr_t chan;
{
	register struct proc *p;
	register struct proc *ep;
	int s;

	p = &proc[0];
	ep = (struct proc *)v.ve_proc;
	do {
		if(p->p_wchan==chan && p->p_stat==SSLEEP) {
			/*
			 * this code looks dumb, but
			 * there is a possible race due
			 * to interrupts.
			 */
			s = spl6();
			if(p->p_wchan == chan)
				setrun(p);
			splx(s);
		}
		p++;
	} while(p != ep);
}

setrq(p)
struct proc *p;
{
	register struct proc *q;
	register s;

	s = spl6();
	for(q=runq; q!=NULL; q=q->p_link)
		if(q == p) {
			printf("proc on q\n");
			//w_trace(1);
			goto out;
		}
	p->p_link = runq;
	runq = p;
out:
	splx(s);
}

/*
 * Set the process running;
 * arrange for it to be swapped in if necessary.
 */
setrun(p)
register struct proc *p;
{

	p->p_wchan = 0;
	p->p_stat = SRUN;
	setrq(p);
	if ((p->p_flag&SLOAD)==0) {
		p->p_time = 0;
		if(runout != 0) {
			runout = 0;
			setrun(&proc[0]);
		}
	} else
		if(p->p_pri < curpri)
			runrun++;
}

/*
 * The main loop of the scheduling (swapping)
 * process.
 * The basic idea is:
 *  see if anyone wants to be swapped in;
 *  swap out processes until there is room;
 *  swap him in;
 *  repeat.
 * The runout flag is set whenever someone is swapped out.
 * Sched sleeps on it awaiting work.
 *
 * Sched sleeps on runin whenever it cannot find enough
 * core (by swapping out or otherwise) to fit the
 * selected swapped process.  It is awakened when the
 * core situation changes and in any case once per second.
 */
sched()
{
	register struct proc *rp, *p;
	register outage, inage;
	register struct proc *inp;
	int maxbad;
	int tmp;

	/*
	 * find user to swap in;
	 * of users ready, select one out longest
	 */

loop:
	spl6();
	outage = -20000;
	for (rp = &proc[0]; rp < (struct proc *)v.ve_proc; rp++)
	if (rp->p_stat==SRUN && (rp->p_flag&SLOAD)==0 &&
	    rp->p_time > outage) {
		p = rp;
		outage = rp->p_time;
	}
	/*
	 * If there is no one there, wait.
	 */
	if (outage == -20000) {
		runout++;
		sleep((caddr_t)&runout, PSWP);
		goto loop;
	}
	spl0();

	/*
	 * See if there is core for that process;
	 * if so, swap it in.
	 */

	if (swapin(p))
		goto loop;

	/*
	 * none found.
	 * look around for core.
	 * Select the largest of those sleeping
	 * at bad priority; if none, select the oldest.
	 */

	spl6();
	inp = p;		/* we are trying to swap this guy in */
	p = NULL;
	maxbad = 0;
	inage = 0;
	for (rp = &proc[0]; rp < (struct proc *)v.ve_proc; rp++) {
		if (rp->p_stat==SZOMB)
			continue;
		if (rp == inp)
			continue;
		if ((rp->p_flag&(SSPART|SLOCK)) == SSPART) {
			p = rp;
			maxbad = 1;
			break;
		}
		if ((rp->p_flag&(SSYS|SLOCK|SLOAD))!=SLOAD)
			continue;
		if (rp->p_textp && rp->p_textp->x_flag&XLOCK)
			continue;
		if (rp->p_stat==SSLEEP || rp->p_stat==SSTOP) {
			tmp = rp->p_pri - PZERO + rp->p_time;
			if (maxbad < tmp) {
				p = rp;
				maxbad = tmp;
			}
		} else if (maxbad<=0 && rp->p_stat==SRUN) {
			tmp = rp->p_time + rp->p_nice - NZERO;
			if (tmp > inage) {
				p = rp;
				inage = tmp;
			}
		}
	}
	spl0();

	/*
	 * Swap found user out if sleeping at bad pri,
	 * or if he has spent at least 2 seconds in core and
	 * the swapped-out process has spent at least 2 seconds out.
	 * Otherwise wait a bit and try again.
	 */
	if (maxbad>0 || (outage>=2 && inage>=2)) {
		int	left, swp;

		p->p_flag &= ~SLOAD;
		left = p->p_size;
		//if (p->p_flag & SSPART)
			left -= p->p_swsize;
			
		swp = p->p_size/10;
		if (swp == 0) swp = 1;
		if (swp > left) swp = left;
		
		//if (left > SWAPSIZE)
		//	left = SWAPSIZE;
		if (swp==0) goto loop; //panic("swp=0");
		xswap(p, -swp, 0, -1); /* partial swap */
		goto loop;
	}
	spl6();
	runin++;
	sleep((caddr_t)&runin, PSWP);
	goto loop;
}
/*
 * Swap a process in.
 */
extern int cnt;
swapin(p)
register struct proc *p;
{
	register struct text *xp;
	unsigned ds, de, se, sz, sn;
	void *upa = p->p_upa;
	pagetable_t pt = p->p_pt;

	
if (cnt++) printf("swapin reentry\n");
//printf("in swapin %d: need %x,%x %x have %x\n", p->p_pid, p->p_size, p->p_tsize, (unsigned)p->p_flag, pgcount);
//ptaudit(p, "c");

	if (xp = p->p_textp)
		xlock(xp);

	/* do a quick check on memory fit here before embarking on detail */
	sn = p->p_swsize + 15;
	if (p->p_pt==0) sn += 7;
	if (xp->x_ccount==0) sn += xp->x_size;
//printf("in swapin %d: need %x 0%o have %x ", p->p_pid, sn, (unsigned)p->p_flag, pgcount);
if (p->p_flag & SLOAD) panic("already loaded");		

	if (sn > pgcount)
		goto nomem2;
//printf("in swapin %d: need %x 0%o have %x ", p->p_pid, sn, (unsigned)p->p_flag, pgcount);
	ds = p->p_size-p->p_ssize;

	if ((p->p_flag & SSPART) == 0) {

		/* was fully swapped out, re-allocate u_area & pagetable.
		 * u area only needs rebuilding after a copy swap
		 */
		if (upa == 0) {
			if ((p->p_upa = kalloc()) == 0)
				goto nomem;
		}
if (p->p_pt) panic("pt not null");
		if ((p->p_pt = pt = new_pt(p->p_upa)) == 0) {
			if (upa == 0 && p->p_upa) {
				kfree(p->p_upa);
				p->p_upa = 0;
			}
			goto nomem;
		}
		if (!upa)
			swap(p, p->p_swaddr+ctod(p->p_size), 0x3fffffd, 1, B_READ);

		/* alloc text segment */
		if (xp && xp->x_ccount > 0 ) {
			if (xp->x_procp == 0 || (xp->x_procp->p_flag & STEXT)==0)
				panic("lost text");
			//printf("uvmc swapin\n");
			//printf("source pid = %d, '%s'\n",  xp->x_procp->p_pid, xp->x_procp->p_upa->u_comm);
			//printf("tsz = %x, flg = %o\n", xp->x_procp->p_tsize, xp->x_procp->p_flag);
			//ptaudit(xp->x_procp->p_pt, "swapin");
			//if (xp->x_procp->p_pid==7) w_trace(1);
			if (uvmcopy(xp->x_procp->p_pt, pt, 0, ctob(xp->x_size), 1) < 0) {
				goto nomem;
			}
		}
		else  {
			if (uvmalloc(pt, 0, ctob(xp->x_size), PTE_R|PTE_X) == 0)
				goto nomem;
		}
		/* alloc data & stack segment */
		de = p->p_tsize + ds;
		if (uvmalloc(pt, ctob(p->p_tsize), ctob(de), PTE_R|PTE_W) == 0)
			goto nomem;
		se = btoc(USRSTACK) - p->p_ssize;
		if (uvmalloc(pt, ctob(se), USRSTACK, PTE_R|PTE_W) == 0)
			goto nomem;
		/* we have memory: now read from disk  */
		if (xp && xp->x_ccount == 0) {
			xp->x_procp = p;	/* make link to loaded proc */
			if ((xp->x_flag&XLOAD)==0)
				swap(p, xp->x_daddr, 0, xp->x_size, B_READ);
		}
		xp->x_ccount++;
		p->p_flag |= STEXT;
		swap(p, p->p_swaddr, p->p_tsize, ds, B_READ);
		swap(p, p->p_swaddr+ctod(ds), se, p->p_ssize, B_READ);

	} else {
		/* partially swapped out, re-allocate missing data + stack pages */
		int dz = ((p->p_swsize > ds) ?  ds : p->p_swsize);
		de = p->p_tsize + dz;
		if (uvmalloc(pt, ctob(p->p_tsize), ctob(de), PTE_R|PTE_W) == 0) {
			goto nomem2;
		}
		sz = (p->p_swsize > ds) ? p->p_swsize - ds : 0;
		if (sz) {
			se = btoc(USRSTACK) - p->p_ssize;
			if (uvmalloc(pt, ctob(se), ctob(se+sz), PTE_R|PTE_W) == 0) {
				uvmdealloc(p->p_pt, ctob(de), ctob(p->p_tsize), 1);
				goto nomem2;
			}
		}

		/* we have memory: now read from disk  */
		swap(p, p->p_swaddr, p->p_tsize, dz, B_READ);
		if (sz)
			swap(p, p->p_swaddr+ctod(ds), se, sz, B_READ);
		p->p_flag &= ~SSPART;
		p->p_swsize = 0;
	}

	if (xp) {
		xunlock(xp);
	}
	mfree(swapmap, ctod(p->p_size+1), p->p_swaddr);
	p->p_swsize = 0;
	p->p_flag |= SLOAD;
	p->p_time = 0;
//ptaudit(p, "d");
//printf("done\n");
cnt--;
	return(1);

nomem:
	//if (p->p_upa != NULL)
	//	kfree(p->p_upa);
	if (p->p_pt != NULL)
		imgzap(p);
nomem2:
	if (xp)
		xunlock(xp);
//ptaudit(p, "e");
//printf("nomem\n");
cnt--;
	return(0);
}

/*
 * put the current process on
 * the Q of running processes and
 * call the scheduler.
 */
qswtch()
{

	sysinfo.qswitch++;
	setrq(u.u_procp);
	swtch();
}

/*
 * This routine is called to reschedule the CPU.
 * if the calling process is not in RUN state,
 * arrangements for it to restart must have
 * been made elsewhere, usually by calling via sleep.
 * There is a race here. A process may become
 * ready after it has been examined.
 * In this case, idle() will be called and
 * will return in at most 1HZ time.
 * i.e. its not worth putting an spl() in.
 */
swtch()
{
	register n;
	register struct proc *p, *q, *pp, *pq;

	/*
	 * If not the idle process, resume the idle process.
	 */
	sysinfo.pswitch++;
	if (u.u_procp != &proc[0]) {
		if (save(u.u_rsav)) {
			return;
		}
		resume(proc[0].p_upa, u.u_qsav);
	}
	/*
	 * The first save returns nonzero when proc 0 is resumed
	 * by another process (above); then the second is not done
	 * and the process-search loop is entered.
	 *
	 * The first save returns 0 when swtch is called in proc 0
	 * from sched().  The second save returns 0 immediately, so
	 * in this case too the process-search loop is entered.
	 * Thus when proc 0 is awakened by being made runnable, it will
	 * find itself and resume itself at rsav, and return to sched().
	 */
	if (save(u.u_qsav)==0 && save(u.u_rsav))
		return;
loop:
	spl6();
	runrun = 0;
	pp = NULL;
	q = NULL;
	n = 128;
	/*
	 * Search for highest-priority runnable process
	 */
	if (p = runq) do {
		if ((p->p_flag&SLOAD) && p->p_pri <= n) {
			pp = p;
			pq = q;
			n = p->p_pri;
		}
		q = p;
	} while (p = p->p_link);
	/*
	 * If no process is runnable, idle.
	 */
	p = pp;
	if(p == NULL) {
		sysinfo.idle++;
		curpri = PIDLE;
		spl0();
		idle();
//setrun(&proc[0]);
		goto loop;
	}
	q = pq;
	if(q == NULL)
		runq = p->p_link;
	else
		q->p_link = p->p_link;
	curpri = n;
	spl0();
	/*
	 * The rsav (ssav) contents are interpreted in the new address space
	 */
	n = p->p_flag&SSWAP;
	p->p_flag &= ~SSWAP;
//printf("resume pid=%d\n", p->p_pid);
	resume(p->p_upa, n? u.u_ssav: u.u_rsav);
}

/*
 * Create a new process-- the internal version of
 * sys fork.
 * It returns 1 in the new process, 0 in the old.
 */
newproc()
{
	register struct proc *rpp, *rip;
	register n;
	struct proc *pend;

	/*
	 * First, just locate a slot for a process
	 * and copy the useful info from this process into it.
	 * The panic "cannot happen" because fork has already
	 * checked for the existence of a slot.
	 */
	rpp = NULL;
retry:
	mpid++;
	if(mpid >= MAXPID) {
		mpid = 0;
		goto retry;
	}
	rip = &proc[0];
	n = v.v_proc;
	do {
		if(rip->p_stat == NULL) {
			if(rpp == NULL)
				rpp = rip;
		} else
			pend = rip;
		if (rip->p_pid==mpid)
			goto retry;
		rip++;
	} while(--n);
	if (rpp==NULL)
		panic("no procs");
	if (rpp > pend)
		pend = rpp;
	pend++;
	v.ve_proc = (char *)pend;

	/*
	 * make proc entry for new proc
	 */

	rip = u.u_procp;
	rpp->p_stat = SRUN;
	rpp->p_clktim = 0;
	rpp->p_flag = SLOAD;
	rpp->p_uid = rip->p_uid;
	rpp->p_pgrp = rip->p_pgrp;
	rpp->p_nice = rip->p_nice;
	rpp->p_textp = rip->p_textp;
	rpp->p_pid = mpid;
	rpp->p_ppid = rip->p_pid;
	rpp->p_time = 0;
	rpp->p_cpu = 0;
	rpp->p_pri = PUSER + rip->p_nice - NZERO;

	/*
	 * make duplicate entries
	 * where needed
	 */

	for(n=0; n<NOFILE; n++)
		if(u.u_ofile[n] != NULL)
			u.u_ofile[n]->f_count++;
	if (rpp->p_textp != NULL) {
		rpp->p_textp->x_count++;
		rpp->p_textp->x_ccount++;
		rpp->p_flag |= STEXT;
	}
	u.u_cdir->i_count++;
	if (u.u_rdir)
		u.u_rdir->i_count++;
	/*
	 * Partially simulate the environment
	 * of the new process so that when it is actually
	 * created (by copying) it will look right.
	 */
	u.u_procp = rpp;
	rpp->p_tsize = rip->p_tsize;
	rpp->p_size = rip->p_size;
	rpp->p_ssize = rip->p_ssize;

	/*
	 * When the resume is executed for the new process,
	 * here's where it will resume.
	 */
	if (save(u.u_ssav)) {
		return(1);
	}

	/*
	 * If there is not enough core for the
	 * new process, swap out the current process to generate the
	 * copy.
	 */
	if (procdup(rip, rpp) == NULL) {
		rip->p_stat = SIDL;
		rpp->p_upa = rip->p_upa;
		rpp->p_pt = rip->p_pt;
		xswap(rpp, 0, 0, -1); /* copy swap */
		rip->p_stat = SRUN;
	}
	u.u_procp = rip;
	setrq(rpp);
	rpp->p_flag |= SSWAP;
	return(0);
}

/*
 * Change the size of the data+stack regions of the process.
 * If the size is shrinking, it's easy-- just release the extra core.
 * If it's growing, and there is core, just allocate it
 * and copy the image, taking care to reset registers to account
 * for the fact that the system's stack has moved.
 * If there is no core, arrange for the process to be swapped
 * out after adjusting the size requirement-- when it comes
 * in, enough core will be allocated.
 *
 * After the expansion, the caller will take care of copying
 * the user's stack towards or away from the data area.
 */
extern int cnt;
void
expand(change, region)
	i64 change;
	int region;
{
	u64 a1, a2;
	unsigned n, oldss, new;
	register struct proc *p;
if (cnt) printf("expand while swapping\n");
	if (change == 0)
		return;

	p = u.u_procp;
	n = p->p_size;
	oldss = p->p_ssize;
	
	switch(region) {
	case SEG_DATA:
		a1 = PGROUNDUP(u.u_dsize + u.u_tsize);
		a2 = PGROUNDUP(u.u_dsize + u.u_tsize + change);
		u.u_dsize += change;
		break;
	case SEG_STACK:
		a1 = PGROUNDDOWN(USRSTACK - u.u_ssize - change);
		a2 = PGROUNDDOWN(USRSTACK - u.u_ssize);
		u.u_ssize += change;
		break;
	case SEG_TEXT:
		panic("SEG_TEXT in expand");
	}
//printf("expand chg=%p, lo=%p, hi=%p, reg=%d\n", change, lo, hi, region);
	if (change < 0) {
//printf("D");
		uvmunmap(p->p_pt, a2, btoc(a1-a2), 1);
	}
	else {
		new = uvmalloc(p->p_pt, a1, a2, PTE_R|PTE_W);
	}

//printf("ts=%p, ds=%p, ss=%p\n", u.u_tsize, u.u_dsize, u.u_ssize);
	p->p_size  = btoc(PGROUNDUP(u.u_dsize) + PGROUNDUP(u.u_ssize));
	p->p_ssize = btoc(PGROUNDUP(u.u_ssize));
//ptaudit(p, "expand");
if (change < 0) return;

	if (new > 0) {
		return;
	}

	/* No core available: swap out and wait for space.
	 * When the resume is executed for the new process
	 * here's where it will resume.
	 */
	if (save(u.u_ssav)) {
		return;
	}
//printf("expswp pid %d, need %d, have %d\n", p->p_pid, p->p_size, pgcount);
//printf("expswp flags = %o\n", p->p_flag);
//printf("expswp cnt=%d, ccnt=%d\n", p->p_textp->x_count, p->p_textp->x_ccount);
	xswap(p, 1, n, oldss); /* expansion swap */
	p->p_flag |= SSWAP;
	qswtch();
	/* no return */
}

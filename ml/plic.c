#include "sys/types.h"

#define PLIC 0x10000000L
#define PLIC_PRIORITY (PLIC + 0x0)
#define PLIC_PENDING (PLIC + 0x1000)
#define PLIC_MENABLE (PLIC + 0x2000)
#define PLIC_SENABLE (PLIC + 0x2080)
#define PLIC_CTRL      (PLIC + 0x1FFFFC)
#define PLIC_MPRIORITY (PLIC + 0x200000)
#define PLIC_SPRIORITY (PLIC + 0x201000)
#define PLIC_MCLAIM (PLIC + 0x200004)
#define PLIC_SCLAIM (PLIC + 0x201004)

#define UART0_IRQ	18
#define VIRTIO0_IRQ	1

void
plicinit(void)
{
	// set desired IRQ priorities non-zero (otherwise disabled).
	*(u32*)(PLIC + UART0_IRQ*4) = 1;
}

void
plicinithart(void)
{
	// set uart's enable bit for this hart's S-mode. 
	*(u32*)PLIC_SENABLE = (1 << UART0_IRQ);
	*(u32*)(PLIC_PRIORITY + (UART0_IRQ * 4)) = 0x1f;

	// set this hart's M- and S-mode priority threshold to 0.
	*(u32*)PLIC_MPRIORITY = 0x0;
	*(u32*)PLIC_SPRIORITY = 0x0;
}

// ask the PLIC what interrupt we should serve.
int
plic_claim(void)
{
	int irq = *(u32*)PLIC_SCLAIM;
	return irq;
}

// tell the PLIC we've served this IRQ.
void
plic_complete(int irq)
{
	*(u32*)PLIC_SCLAIM = irq;
}


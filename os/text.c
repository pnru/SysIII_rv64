#include "sys/param.h"
#include "sys/systm.h"
#include "sys/map.h"
#include "sys/dir.h"
#include "sys/user.h"
#include "sys/proc.h"
#include "sys/text.h"
#include "sys/inode.h"
#include "sys/buf.h"
#include "sys/seg.h"
#include "sys/var.h"
#include "sys/vm.h"


/*
 * Swap out process p.
 * The ff flag causes its core to be freed--
 * it may be off when called to create an image for a
 * child process in newproc.
 * On a partial swap ff is the negative number of blocks to be swapped.
 * Os is the old size  of the process,
 * and is supplied during core expansion swaps.
 * Ss is the old stack size for core expansion swaps.
 *
 * panic: out of swap space
 */
int cnt;
xswap(p, ff, os, ss)
register struct proc *p;
{

	int ts, base, sz;
	int free, a;

if (cnt++) printf("xswap reentry\n");
if (p->p_flag & SLOCK) panic("xswap on lock");
	
//printf("in xswap %d: %d, %d, %d (%d) ", p->p_pid, ff, os, ss, p->p_ssize);
//printf("ps=%x, pss=%x, sws=%x\n", p->p_size, p->p_ssize, p->p_swsize);
//ptaudit(p, "xswap", os, ss);

	p->p_flag |= SLOCK;
//if (p->p_ssize==0) printf("ss=0, pid=%d\n", p->p_pid);
	ts = p->p_tsize;
	if (os ==  0 && ss != 0) os = p->p_size;
	if (ss == -1) ss = p->p_ssize;

	free = (ff != 0);
	if (ff >= 0)
		ff = -os;
	ff = -ff;

	if (ff > (os - p->p_swsize))
		panic("partial swap size exceeds process size");
	
	/* alloc swap space (for new size if expanding) */
	if ((p->p_flag & SSPART) == 0)  {
		a = malloc(swapmap, ctod(p->p_size + 1));
		if(a == NULL)
			panic("out of swap space");
		p->p_swaddr = a;
		p->p_swsize = 0;
	}
	/* (partially) swap out data segment */
	if (p->p_swsize < os-ss) {
		base = p->p_tsize + p->p_swsize;
		sz = os-ss-p->p_swsize;
		if (sz > ff)
			sz = ff;
//printf("sz = %d, swsz = %d, base = %x\n", p->p_size, p->p_swsize, base);
		swap(p, p->p_swaddr + ctod(p->p_swsize), base, sz, B_WRITE);
		if (free)
			uvmunmap(p->p_pt, ctob(base), sz, 1);
		p->p_swsize += sz;
		ff -= sz;
	}
	/* skip expansion gap (if any) */
	if (p->p_swsize == os-ss) 
		p->p_swsize += p->p_size - os;
	
	/* (partially) swap out stack segment */
	if (ff>0) {
		base = btoc(USRSTACK) - (p->p_size - p->p_swsize);
		swap(p, p->p_swaddr + ctod(p->p_swsize), base, ff, B_WRITE);
		if (free)
			uvmunmap(p->p_pt, ctob(base), ff, 1);
		p->p_swsize += ff;
	}
	p->p_flag |= SSPART;
	/* swap out text, u-area and zap pagetable once fully swapped out */
	if (p->p_swsize == p->p_size) {
		p->p_flag &= ~SSPART;
		if (p->p_textp) {
			if (free)
				xccdec(p->p_textp, p);
			else {
				p->p_textp->x_ccount--;
				swap(p, p->p_swaddr+ctod(p->p_size), 0x3fffffd, 1, B_WRITE);
				p->p_upa = NULL;
			}
		}
		if (free) {
			imgzap(p);
			//w_trace(1);
			//kfree(p->p_upa);
			//p->p_upa = NULL;
		}
		else
			p->p_pt = 0;
	}
	p->p_flag &= ~SLOAD;
	p->p_flag &= ~SLOCK;
	p->p_time = 0;
	if(runout) {
		runout = 0;
		wakeup((caddr_t)&runout);
	}
//ptaudit(p,"b");
//printf("done %x\n", pgcount);
cnt--;
}


/*
 * relinquish use of the shared text segment
 * of a process.
 */
xfree()
{
	register struct text *xp;
	register struct inode *ip;
	register struct proc *p = u.u_procp;

	if((xp=p->p_textp) == NULL)
		return;
	xlock(xp);
	xp->x_flag &= ~XLOCK;
	p->p_textp = NULL;
	p->p_flag &= ~STEXT;
	ip = xp->x_iptr;
//printf("pid=%d, c=%d, cc=%d\n", p->p_pid, xp->x_count, xp->x_ccount);
	if(--xp->x_count==0 && (ip->i_mode&ISVTX)==0) {
		xp->x_iptr = NULL;
		mfree(swapmap, ctod(xp->x_size), xp->x_daddr);
//printf("G");
		uvmdealloc(p->p_pt, ctob(xp->x_size), 0, 1);
		ip->i_flag &= ~ITEXT;
		if (ip->i_flag&ILOCK)
			ip->i_count--;
		else
			iput(ip);
	} else {
		xccdec(xp, u.u_procp);
//printf("past a\n");
	}
}

/*
 * Attach to a shared text segment.
 * If there is no shared text, just return.
 * If there is, hook up to it:
 * if it is not currently being used, it has to be read
 * in from the inode (ip); the written bit is set to force it
 * to be written out as appropriate.
 * If it is being used, but is not currently in core,
 * a swap has to be done to get it back.
 */
xalloc(ip)
register struct inode *ip;
{
	register struct text *xp, *xp1;
	register u64 ts;
	register struct proc *p = u.u_procp;

//printf("xalloc ip=%p, p=%p\n", ip, p);
	ts = btoc(u.u_tsize);
	if(ts == 0)
		return;

	xp1 = NULL;
	for (xp = &text[0]; xp < (struct text *)v.ve_text; xp++) {
//printf("text[%p]: ip = %p p = %p\n", xp, xp->x_iptr, xp->x_procp);
		if(xp->x_iptr == NULL) {
			if(xp1 == NULL)
				xp1 = xp;
			continue;
		}
		if(xp->x_iptr == ip) {
			xlock(xp);
			xp->x_count++;
			p->p_textp = xp;
			if (xp->x_ccount == 0)
				xexpand(xp);
			else {
				xp->x_ccount++;
				p->p_flag |= STEXT;
				if (xp->x_procp == 0)
					panic("lost text");
				uvmcopy(xp->x_procp->p_pt, p->p_pt, 0, ts * PGSIZE, 1); /* shallow copy */
				/* what if error? */
			}
			xunlock(xp);
			return;
		}
	}
	if((xp=xp1) == NULL) {
		printf("out of text");
		psignal(u.u_procp, SIGKILL);
		return;
	}
	xp->x_flag = XLOAD|XLOCK;
	xp->x_count = 1;
	xp->x_ccount = 0;
	xp->x_iptr = ip;
//printf("iptr=%p %p\n", xp->x_iptr, ip);
	ip->i_flag |= ITEXT;
	ip->i_count++;
	xp->x_size = ts;
	if((xp->x_daddr = malloc(swapmap, ctod(ts))) == NULL)
		panic("out of swap space");
	p->p_textp = xp;
	xexpand(xp);
}

/*
 * Assure core for text segment
 * Text must be locked to keep someone else from
 * freeing it in the meantime.
 * x_ccount must be 0.
 */
xexpand(xp)
register struct text *xp;
{
	register struct proc *p = u.u_procp;
//printf("xexpand %x\n", xp->x_size);
	if (uvmalloc(p->p_pt, 0, xp->x_size * PGSIZE, PTE_R|PTE_X) != NULL) {
		xp->x_procp = p;
		if ((xp->x_flag&XLOAD)==0)
			swap(p, xp->x_daddr, 0, xp->x_size, B_READ);
		xp->x_ccount++;
		u.u_procp->p_flag |= STEXT;
		xunlock(xp);
		return;
	}
	if (save(u.u_ssav)) {
		return;
	}
	xswap(u.u_procp, 1, 0, -1); /* full swap */
	p->p_tsize = btoc(u.u_tsize);
	xunlock(xp);
	u.u_procp->p_flag |= SSWAP;
	qswtch();
	/* no return */
}

/*
 * Lock and unlock a text segment from swapping
 */
xlock(xp)
register struct text *xp;
{

	while(xp->x_flag&XLOCK) {
		xp->x_flag |= XWANT;
		sleep((caddr_t)xp, PSWP);
	}
	xp->x_flag |= XLOCK;
}

xunlock(xp)
register struct text *xp;
{

	if (xp->x_flag&XWANT)
		wakeup((caddr_t)xp);
	xp->x_flag &= ~(XLOCK|XWANT);
}

/*
 * Decrement the in-core usage count of a shared text segment.
 * When it drops to zero, free the core space.
 */
xccdec(xp, p)
register struct text *xp;
register struct proc *p;
{
	struct proc *np;
	int i;
	pte_t *ptep;

	if (xp==NULL || xp->x_ccount==0)
		return;
	xlock(xp);
	p->p_flag &= ~STEXT;
	if (--xp->x_ccount==0) {
		if (xp->x_flag&XWRIT) {
			xp->x_flag &= ~XWRIT;
			swap(p, xp->x_daddr, 0, xp->x_size, B_WRITE);
		}
//printf("E");
		uvmdealloc(p->p_pt, ctob(xp->x_size), 0, 1);
		xp->x_procp = 0;
		xunlock(xp);
		return;
	}
	if (xp->x_procp == p) {
		xp->x_procp = 0;	/* page table no longer valid */
		for (np= &proc[1]; np<(struct proc *)v.ve_proc; np++) {
			if (np->p_textp==xp && np->p_pt!=0 && (np->p_flag&STEXT)){ // !(np->p_flag&SSPART)) {
				uvmflip(np->p_pt, xp->x_size);
				uvmflip(p->p_pt, xp->x_size);
				p->p_textp == 0;
				xp->x_procp = np;
				break;
			}
		}
		if (xp->x_procp==0)
			panic("text not transferred");
	}
//printf("xcc2 pid %d, flags %o\n", p->p_pid, p->p_flag);
	uvmdealloc(p->p_pt, ctob(xp->x_size), 0, 0); /* deallocate in pt only */
	xunlock(xp);
}

/*
 * free the swap image of all unused saved-text text segments
 * which are from device dev (used by umount system call).
 */
xumount(dev)
register dev;
{
	register struct text *xp;

	for (xp = &text[0]; xp < (struct text *)v.ve_text; xp++) 
		if (xp->x_iptr!=NULL && dev==xp->x_iptr->i_dev)
			xuntext(xp);
}

/*
 * remove a shared text segment from the text table, if possible.
 */
xrele(ip)
register struct inode *ip;
{
	register struct text *xp;

	if ((ip->i_flag&ITEXT) == 0)
		return;
	for (xp = &text[0]; xp < (struct text *)v.ve_text; xp++)
		if (ip==xp->x_iptr)
			xuntext(xp);
}

/*
 * remove text image from the text table.
 * the use count must be zero.
 */
xuntext(xp)
register struct text *xp;
{
	register struct inode *ip;

	xlock(xp);
	if (xp->x_count) {
		xunlock(xp);
		return;
	}
	ip = xp->x_iptr;
	xp->x_flag &= ~XLOCK;
	xp->x_iptr = NULL;
	mfree(swapmap, ctod(xp->x_size), xp->x_daddr);
	ip->i_flag &= ~ITEXT;
	if (ip->i_flag&ILOCK)
		ip->i_count--;
	else
		iput(ip);
}

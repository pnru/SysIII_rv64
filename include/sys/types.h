
/* standard sizes */
typedef long long		i64;
typedef long			i32;
typedef short			i16;
typedef signed char		i8;

typedef unsigned long long	u64;
typedef unsigned long		u32;
typedef unsigned short		u16;
typedef unsigned char		u8;

typedef	struct { int r[1]; } *physadr;

/* named types */
typedef	i32	daddr_t;
typedef	char *	caddr_t;
typedef	u16	ushort;
typedef	u16	ino_t;
typedef i16	cnt_t;
typedef	i32	time_t;
typedef	i32	label_t[14];
typedef	i16	dev_t;
typedef	i32	off_t;
typedef	i64	paddr_t;
typedef u64	word;


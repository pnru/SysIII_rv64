
#include "sd.h"

/* For a description of PIO configuration see section 9.7 of the D1 manual
	*
	* There four relevant control words per port:
	*   CFG sets the port/pin function		(array with  8 pins per word)
	*   DRV sets the drive strength		(array with 16 pins per word)
	*   PULL sets the pull direction		(id.)
	*   DATA gives r/w access to the pin status	(one bit per pin)
	*/
	
// PIO configuration starts at BASE and ports are spaced SZ apart
#define PIO_BASE	0x02000000
#define PIO_SZ		0x30

// Offsets of each control word
#define CFG0		0x00
#define DATA		0x10
#define DRV0		0x14
#define PULL0		0x24


// Set the configuration for a list of pins, using an array of
// gpio_set_t settings (one entry per pin)
//
int set_gpio(gpio_set_t *list, int len, int set_mode)
{
	gpio_set_t	*cur;
	int		i, offs, port, pin, idx4, idx2, o_mode;
	uint32_t	val;
	uint64_t	base;
	void		*addr;

	for (i = 0; i < len; i++) {
	
		cur = list + i;
		port = cur->port - 1;
		pin  = cur->pin;
		
		// only handle port PB through PG, pins 0..31
		if ( port<1 || port>6 || pin<0 || pin>31 ) {
			printf("GPIO: bad port/pin number\n");
			return -1;
		}

		base = PIO_BASE + port*PIO_SZ;
		idx4 = (pin >> 3) << 2; // word index for  8 pins per word (4 bits per pin)
		idx2 = (pin >> 4) << 2; // word index for 16 pins per word (2 bits per pin)

		// update pin function, set_mode==0 is leave as-is
		addr = (void*)(base + CFG0 + idx4);
		offs = (pin & 0x7) << 2;
		val  = readl(addr);
		if (set_mode) {
			val &= ~(0xf << offs);
			val |= (cur->mode & 0xf) << offs;
			writel(addr, val);
		}
		o_mode = ((val >> offs) & 0xf) == 1;

		// update pull direction, -1 is leave as-is
		addr = (void*)(base + PULL0 + idx2);
		offs = (pin & 0xf) << 1;
		if (cur->pull >= 0) {
			val  = readl(addr) & ~(0x3 << offs);
			val |= (cur->pull & 0x3) << offs;
			writel(addr, val);
		}

		// update drive strength, -1 is leave as-is
		addr = (void*)(base + DRV0 + idx2);
		// same: offs = (pin & 0xf) << 1;
		if (cur->drv >= 0) {
			val  = readl(addr) & ~(0x3 << offs);
			val |= (cur->drv & 0x3) << offs;
			writel(addr, val);
		}

		// update output data, -1 or mode!=output is leave as-is
		addr = (void*)(base + DATA);
		if (o_mode & cur->data >= 0) {
			val  = readl(addr) & ~(0x1 << pin);
			val |= (cur->data & 0x1) << pin;
			writel(addr, val);
		}
	}
	return 0;
}

void*
memmove(void *dst, const void *src, unsigned n)
{
	const char *s;
	char *d;

	s = src;
	d = dst;
	if(s < d && s + n > d){
		s += n;
		d += n;
		while(n-- > 0)
			*--d = *--s;
	} else
		while(n-- > 0)
			*d++ = *s++;

	return dst;
}

char*
strncpy(char *s, const char *t, int n)
{
	char *os;

	os = s;
	while(n-- > 0 && (*s++ = *t++) != 0)
		;
	while(n-- > 0)
		*s++ = 0;
	return os;
}

extern uint64_t hw_timer;

void sdelay(unsigned long us)
{   
	uint64_t t1 = hw_timer;
	uint64_t t2 = t1 + us;
	do {
		t1 = hw_timer;
	} while(t2 >= t1);
}

void mdelay(uint64_t ms)
{   
	sdelay(ms*1000);
}

// assembly assists in entry.s
void dcache_cpa(uint64_t);
void dcache_ipa(uint64_t);
void sync_is(void);

#define L1_CACHE_BYTES	64

void flush_dcache_range(void* start, uint64_t len)
{
	uint64_t i   = (uint64_t)start & ~(L1_CACHE_BYTES - 1);
	uint64_t end = i + len;
	
	// flush each cache line in range individually
	for (; i < end; i += L1_CACHE_BYTES)	dcache_cpa(i);
	sync_is();
}

void invalidate_dcache_range(void* start, uint64_t len)
{
	uint64_t i   = (uint64_t)start & ~(L1_CACHE_BYTES - 1);
	uint64_t end = i + len;
	
	// invalidate each cache line in range individually
	for (; i < end; i += L1_CACHE_BYTES)	dcache_ipa(i);
	sync_is();
}


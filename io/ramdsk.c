/*
 * Ram disk driver
 */

#include "sys/types.h"
#include "sys/param.h"
#include "sys/dir.h"
#include "sys/user.h"
#include "sys/buf.h"
#include "sys/elog.h"
#include "sys/iobuf.h"
#include "sys/systm.h"
#include "sys/vm.h"

struct	iobuf	rdtab;

rdopen(int dev)
{
}

rdclose(int dev)
{
}

rdstrategy(struct buf *bp)
{
	if ( (bp->b_blkno >= swplo + nswap) || ((bp->b_blkno < swplo)) ||
	     (bp->b_blkno < 0) ) 
	{
printf("ramdisk blkno = %d\n", bp->b_blkno);
		bp->b_flags |= B_ERROR;
		iodone(bp);
		return;
	}

	bp->av_forw = 0;
	/* link buffer to end of service queue */
	spl7();
	if (rdtab.b_actf == 0)
		rdtab.b_actf = bp;
	else
		rdtab.b_actl->av_forw = bp;
	rdtab.b_actl = bp;
	/* fdtab.b_active is always 0 for now */
	if (rdtab.b_active == 0)
		rdstart();
	spl0();
}

static int bkread(int block, char* to, int len)
{
	u8 *f;
//printf("RAMDSK RD %d, %d, %p\n", block, len, to);
	f = (u8*)(PHYSTOP + (block-swplo) * BSIZE);
			     
	while(len--) *to++ = *f++;
	return 0;
}

static int bkwrite(int block, char* from, int len)
{
	u8 *t;
//printf("RAMDSK WR %d, %d, %p\n", block, len, from);
	t = (u8*)(PHYSTOP + (block-swplo) * BSIZE);
			     
	while(len--) *t++ = *from++;
	return 0;
}

rdstart(void)
{
	register struct buf *bp;
	int rc;

	/* Unlink immediately; there is always 0 or 1 request in the queue */
	if ((bp = rdtab.b_actf) == 0)
		return;
	rdtab.b_actf = bp->av_forw;
	
	/* Read or write the buffer to the CF Card. Normal I/O to a kernel
	 * disk buffer is easy, direct I/O to user space (swapping, "raw" disk
	 * access) requires juggling the page map.
	 */
	if(bp->b_flags & B_PHYS) {
		printf("direct I/O not yet implemented\n");
	} else {
		if(bp->b_flags & B_READ)
		  rc = bkread(bp->b_blkno, (char*)paddr(bp),  bp->b_bcount);
		else
		  rc = bkwrite(bp->b_blkno, (char*)paddr(bp), bp->b_bcount);
	}
	if(rc) 
		bp->b_flags |= B_ERROR;
	iodone(bp);
}

rdintr(void)
{
}

struct	buf	rdbuf;

rdread(int dev)
{
		physio(rdstrategy, &rdbuf, dev, B_READ);
}

rdwrite(int dev)
{
		physio(rdstrategy, &rdbuf, dev, B_WRITE);
}


#include <sys/types.h>
#include <sys/stat.h>

int
remove(file)
	const char *file;
{
	struct stat sb;

	if (stat(file, &sb) < 0)
		return (-1);
	if (sb.st_mode & S_IFDIR)
		return (rmdir(file));
	return (unlink(file));
}


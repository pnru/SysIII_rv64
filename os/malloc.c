#include "sys/types.h"
#include "sys/param.h"
#include "sys/systm.h"
#include "sys/map.h"
#include "sys/page.h"
#include "sys/mem.h"

/*
 * Allocate 'size' units from the given map.
 * Return the base of the allocated space.
 * In a map, the addresses are increasing and the
 * list is terminated by a 0 size.
 * The swap map unit is 512 bytes.
 * Algorithm is first-fit.
 */
malloc(mp, size)
struct map *mp;
{
	register unsigned int a;
	register struct map *bp;
	
//printf("swp alloc %d\n", size);

	for (bp=mp; bp->m_size; bp++) {
		if (bp->m_size >= size) {
			a = bp->m_addr;
			bp->m_addr += size;
			if ((bp->m_size -= size) == 0) {
				do {
					bp++;
					(bp-1)->m_addr = bp->m_addr;
				} while ((bp-1)->m_size = bp->m_size);
			}
			return(a);
		}
	}
	return(0);
}

/*
 * Free the previously allocated space aa
 * of size units into the specified map.
 * Sort aa into map and combine on
 * one or both ends if possible.
 */
mfree(mp, size, a)
struct map *mp;
unsigned int a;
{
	register struct map *bp;
	register unsigned int t;

//printf("swp free %d\n", size);
	bp = mp;
	for (; bp->m_addr<=a && bp->m_size!=0; bp++);
	if (bp>mp && (bp-1)->m_addr+(bp-1)->m_size == a) {
		(bp-1)->m_size += size;
		if (a+size == bp->m_addr) {
			(bp-1)->m_size += bp->m_size;
			while (bp->m_size) {
				bp++;
				(bp-1)->m_addr = bp->m_addr;
				(bp-1)->m_size = bp->m_size;
			}
		}
	} else {
		if (a+size == bp->m_addr && bp->m_size) {
			bp->m_addr -= size;
			bp->m_size += size;
		} else if (size) {
			do {
				t = bp->m_addr;
				bp->m_addr = a;
				a = t;
				t = bp->m_size;
				bp->m_size = size;
				bp++;
			} while (size = t);
		}
	}
}

/* Physical kernel memory allocator, for user processes,
 * kernel stacks, page-table pages, and pipe buffers.
 * Allocates whole 4096-byte pages.
 */

extern char end[]; // first address after kernel.

struct run {
	struct run *next;
};

struct {
//	struct spinlock lock;
	struct run *freelist;
} kmem;

/* 4MB for now */
#define PHYSTOP (0x40400000)

#define PGSIZE  4096
#define PGROUNDUP(sz)  (((sz)+PGSIZE-1) & ~(PGSIZE-1))

int dospl0 =0;
unsigned pgcount = 0;

int isfree(void *pa)
{
	struct run *r;
	
	r = kmem.freelist;
	while(r) {
		if (r == (struct run*)pa)
			return 1;
		r = r->next;
	}
	return 0;
}

/* Free the page of physical memory pointed at by v, which normally should
 * have been returned by a call to kalloc().  (The exception is when
 * initializing the allocator; see kinit below.)
 */
void kfree(void *pa)
{
	struct run *r;
	int lck;
//printf("kfree %p\n", pa);
	if(((u64)pa % PGSIZE) != 0 || (char*)pa < (end+4096) || (u64)pa >= PHYSTOP) {
printf("kfree %p\n", pa);
		panic("kfree");
	}
	if (dospl0 && isfree(pa)) {
		//w_pnr(0);
		panic("double free!!");
	}

	/* Fill with junk to catch dangling refs. */
	memset(pa, 1, 4096);
	r = (struct run*)pa;

	lck = spl7();
	r->next = kmem.freelist;
	kmem.freelist = r;
	pgcount++;
	splx(lck);
}

void freerange(void *pa_start, void *pa_end)
{
	char *p;
	p = (char*)PGROUNDUP((u64)pa_start);
	for(; p + PGSIZE <= (char*)pa_end; p += PGSIZE) {
//printf("kfree %p\n", p);
		kfree(p);
	}
}

u64 meminit()
{
	freerange(end+4096, (void*)PHYSTOP);
	dospl0 = 1;
	return (PHYSTOP - (u64)end);
}

int memcheck(char *nm)
{
	struct run *r;
	unsigned count = 0, s;
	
//	s = spl7();
	r = kmem.freelist;
	while(r) {
//printf("pg = %p\n", r);
		count++;
		r = r->next;
	}
//	splx(s);
	if (1 || count!=pgcount) {
		printf("memcheck %s pgcount=%x, listsz=%x\n", nm, pgcount, count);
	}
	return count == pgcount ? 1 : 0;
}

/* Allocate one 4096-byte page of physical memory. Returns a pointer that
 * the kernel can use. Returns 0 if the memory cannot be allocated.
 */
void *kalloc(void)
{
	struct run *r;
	int lck;

	lck = spl7();
	r = kmem.freelist;
if (r==0)
//printf("OOM!!\n")
; else if(((u64)r % PGSIZE) != 0 || (char*)r < end || (u64)r >= PHYSTOP)
  printf("bad freelist %p\n", r);
	if(r)
		kmem.freelist = r->next;
	splx(lck);

	if(r) {
		memset((char*)r, 0, PGSIZE); // fill with zero
		pgcount--;
	}
	return (void*)r;
}


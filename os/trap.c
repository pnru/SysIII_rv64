#include "sys/types.h"
#include "sys/param.h"
#include "sys/systm.h"
#include "sys/dir.h"
#include "sys/user.h"
#include "sys/proc.h"
#include "sys/psl.h"
#include "sys/trap.h"
#include "sys/seg.h"
#include "sys/vm.h"
#include "sys/sysinfo.h"

#define	USER	0x100		/* user-mode flag added to type */
#define	NSYSENT	64

int echo();

/*
 * Called from the trap handler when a processor trap occurs.
 */
trap(u64 type, u64 code, u64 pc, u64 ps)
{
	register i;
	time_t syst;

	syst = u.u_stime;
	u.u_ar0 = &u.u_pcb.R8;
	
	switch (type) {

	/*
	 * Trap not expected.
	 * Usually a kernel mode bus error.
	 */
	default:
		//w_trace(0);
		//w_pnr(0);
		printf("KERNEL TRAP\n");
		printf("user = %p\n", u.u_procp->p_upa);
		printf("ps   = %p\n", ps);
		printf("pc   = %p\n", pc);
		printf("type = %p\n", type);
		printf("code = %p\n", code);
		printf("pid  = %d\n", u.u_procp->p_pid);
		panic("trap");
	
	/* misaligned or access fault */
	case USER + 0:
	case USER + 1:
	case USER + 4:
	case USER + 5:
	case USER + 6:
	case USER + 7:
		i = SIGBUS;
		break;

	/* illegal instruction */
	case USER + 2:
		printf("illegal instruction\n");
		i = SIGILL;
		break;

	/* system call */
	case USER + 8:	/* user                  */
	case USER + 9:	/* supervisor            */
	case USER + 10: /* hypervisor / reserved */
	case USER + 11: /* machine               */
	{
		register u64 *a;
		register struct sysent *callp;

		sysinfo.syscall++;
		u.u_error = 0;
		u.u_pcb.epc += 4; /* past ebreak */
		u.u_pcb.R4 = 0; /* clear error flag */
		i = u.u_pcb.R15 & 0377;
		if (i >= NSYSENT) {
			i = 0;
//		} else if (i==0) {	/* indirect */
//			i = fuword(a++)&0377;
//			if (i >= NSYSENT)
//				i = 0;
		}
//printf("**** syscall %d\n", i);
		if(i==0) {
			panic("indirect sys call");
			i = 1;
		}
		callp = &sysent[i];
		for(i=0; i<callp->sy_narg; i++) {
			u.u_arg[i] = u.u_ar0[i];
		}
		u.u_dirp = (caddr_t)u.u_arg[0];
		u.u_rval1 = 0;
		u.u_rval2 = u.u_ar0[1];
		u.u_ap = u.u_arg;
		if (setjmp(u.u_qsav)) {
			if (u.u_error==0)
				u.u_error = EINTR;
		} else {
			(*callp->sy_call)();
		}
		if (u.u_error) {
			u.u_ar0[0] = u.u_error;
			u.u_pcb.R4 = 1; /* set error flag */
			if (++u.u_errcnt > 16) {
				u.u_errcnt = 0;
				runrun++;
			}
		} else {
			u.u_ar0[0] = u.u_rval1;
			u.u_ar0[1] = u.u_rval2;
		}
//printf("**** done\n");
		register struct proc *pp;

		pp = u.u_procp;
		pp->p_pri = (pp->p_cpu>>1) + PUSER + pp->p_nice - NZERO;
		curpri = pp->p_pri;
		if (runrun == 0)
			goto out;

		sysinfo.preempt++;
		qswtch();
		goto out;
	}
	
	/*
	 * Segmentation exceptions. If the user SP is below the stack segment,
	 * grow the stack automatically.
	 */
	case USER + 12:
	case USER + 13:
	case USER + 14:
	case USER + 15:
		if(grow(u.u_pcb.R2)) // || grow(code))
			goto out;
{
struct proc *p = u.u_procp;
printf("segfault PID=%d TYPE=%p PC=%p ADDR=%p (t:%x,d:%x %s)\n", p->p_pid, type, pc, code, p->p_tsize, p->p_size-p->p_ssize, &(u.u_comm[0]));
}
//w_trace(1);
		i = SIGSEGV;
		break;

	/* breakpoints & tracing */
	case USER + 3:
	//	ps &= ~PS_T;	/* turn off trace bit */
		i = SIGTRAP;
		break;

	}
	psignal(u.u_procp, i);

out:
	if(issig())
		psig();

	if(u.u_prof.pr_scale)
		addupc((caddr_t)pc, &u.u_prof, (int)(u.u_stime-syst));
}

/*
 * nonexistent system call-- signal bad system call.
 */
nosys()
{
	psignal(u.u_procp, SIGSYS);
}

/*
 * Ignored system call
 */
nullsys()
{
}

/*
 *  Test call print arg0
 */
#include "sys/var.h"
echo()
{
	struct proc *p;
	
	printf("\n");
	for (p = &proc[1]; p < (struct proc *)v.ve_proc; p++) {
		printf("proc pid %d, stat %d, wchan %x\n", p->p_pid, p->p_stat, p->p_wchan);
	}
	printf("\n");
//	printf("echo: %p\n", u.u_pcb.R8);
}

u64 r_sstatus();
u64 r_scause();
u64 r_stval();
u64 r_sepc();

void userjmp(void);

int  spl0(void);
int  spl7(void);
void idle(void);

#define PLIC_SCLAIM	(PLIC + 0x201004)
#define UART0_IRQ	18
#define VIRTIO0_IRQ	2

u64 r_sip();
void w_sip(u64 x);

/* Dispatch exceptions to handler routines. Main two categories
 * are interrupts (scause < 0) and traps from user space. Traps
 * from kernel space are an error condition.
 */
void trapdisp(void)
{
	u64 sepc    = r_sepc();
	u64 sstatus = r_sstatus();
	u64 scause  = r_scause();
	u64 stval   = r_stval();
	u32 irq;
	int user = !(sstatus & SSTATUS_SPP);

	//spl7();
//w_trace(0);

	if (user) {
		scause |= USER;
		u.u_pcb.epc = sepc;
	}
	
	if((i64)scause<0) {
		switch(scause & 0xff) {
		case 1:
//printf("~");
			/* clock interrupt */
			w_sip(r_sip() & ~2);
			clock(sepc, sstatus);
			break;
		case 9:
			/* uart interrupt */
			irq = *(u32*)PLIC_SCLAIM;
//printf("uirq=%d\n", irq);
//w_pnr(irq);
			switch(irq) {
			case UART0_IRQ:		uaint(0); break;
			case VIRTIO0_IRQ:	vtintr(); break;
			default:	panic("unexpected interrupt");
			}
			*(u32*)PLIC_SCLAIM = irq;
//printf("uack=%d\n", irq);
			break;
		default:
			printf("scause = %p\n", scause);
			panic("unexpected int\n");
		}
	}
	else {
		spl0();
		trap(scause, stval, sepc, sstatus);
	}
	if (user) {
		if (runrun)
			qswtch();
		userjmp();
		/* does not return */
	}
}

#define SSTATUS_SPIE	(1L << 5)	// Supervisor Previous Interrupt Enable

void userjmp(void)
{
	extern char trampoline[], userret[];
	extern void w_sepc(u64 x);
	extern void w_sstatus(u64 x);
	extern u64  r_satp();

	spl7();

	/* set up the u.u_pcb values that uservec will need when
	 * the process next re-enters the kernel.
	 */
	u.u_pcb.kernel_satp   = r_satp(); 
	u.u_pcb.kernel_sp     = (u64)&u + PGSIZE - 16; 
	u.u_pcb.kernel_addr   = (u64)trapdisp;
//	u.u_pcb.kernel_hartid = r_tp(); 
	u.u_pcb.kernel_gp     = r_gp();

	/* prepare for the SRET jump to user mode */
	u64 x = r_sstatus();
	x &= ~SSTATUS_SPP;  // clear SPP to 0 for user mode
	x |=  SSTATUS_SPIE; // enable interrupts in user mode
	w_sstatus(x);
	w_sepc(u.u_pcb.epc);

	/* prepare the new page table vector */
	u64 satp = MAKE_SATP(u.u_procp->p_pt);

	/* jump to userret (on the trampoline page), which 
	 * switches to the user page table, restores user registers,
	 * and switches to user mode with SRET.
	 */
	u64 fn = TRAMPOLINE + (userret - trampoline);
//if ((u.u_comm[0] == 'j' && u.u_comm[1]== '.')) w_trace(1);
//printf("run pid %d\n", u.u_procp->p_pid);
	((void (*)(void*,u64))fn)(&u, satp);
	/* does not return */
}


char *
memccpy(t, f, c, n)
	register char *t, *f;
	register c, n;
{
	while (--n >= 0)
		if ((*t++ = *f++) == c)
			return (t);
	return (0);
}


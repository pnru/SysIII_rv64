	//
        // Handle signal, with register save and restore in user mode.
        //
	
#define NSIG	20

GLOBL __sigvec<>(SB), $(NSIG*8)

TEXT signal(SB),$-8

	// signal# in range?
	MOV	$NSIG,R9
	BGT	R8,R9,sigok
	MOV	$22,R9			// EINVAL
	MOV	R9,errno(SB)
	MOV	$-1,R8
	RET

sigok:
	MOV	R8,R10			// find sigvec slot
	SLL	$3,R10
	MOV	$__sigvec<>(SB),R11
	ADD	R10,R11,R10
	MOV	0(R10),R11		// old vector to R11

	MOV	16(R2),R9		// fetch new vector
	MOV	R9,0(R10)		// store in sigvec table
	BEQ	R9,R0,useasis		// if zero, use as is
	AND	$1,R9,R4
	BNE	R4,R0,useasis		// if odd, use as is	
	MOV	$__tvect<>(SB),R9	// else substitute tvect address

useasis:
	MOV	$48,R15			// do ssig system call
	ECALL
	BLT	R4,R0,sigerr
	AND	$1,R11,R4		// if old vector was odd, use that value
	BEQ	R4,R0,usesys
	MOV	R11,R8
usesys:
	RET

sigerr:
	MOV	R8,errno(SB)
	MOV	$-1,R8
	RET
	
	//
	// Receive a signal
	//

TEXT __tvect<>(SB),$-8
	// save the C registers (R1-R15, excl. R2, R3, R8)
	SUB	$96,R2,R2
        MOV	R1,    0(R2)
	MOV	R4,    8(R2)
	MOV	R5,   16(R2)
        MOV	R6,   24(R2)
        MOV	R7,   32(R2)
	// skip R8, kernel has put signal# there
        MOV	R9,   40(R2)
        MOV	R10,  48(R2)
        MOV	R11,  56(R2)
        MOV	R12,  64(R2)
        MOV	R13,  72(R2)
        MOV	R14,  80(R2)
        MOV	R15,  88(R2)

	// call the real trap handler
	SLL	$3,R8
	MOV	$__sigvec<>(SB),R9
	ADD	R8,R9,R9
	MOV	0(R9),R9
	SRL	$3,R8
        JAL	,0(R9)

        // restore registers.
	MOV  	0(R2), R1
        MOV  	8(R2), R4
        MOV 	16(R2), R5
        MOV 	24(R2), R6
        MOV 	32(R2), R7
        MOV 	40(R2), R9
        MOV 	48(R2), R10
        MOV 	56(R2), R11
        MOV 	64(R2), R12
        MOV 	72(R2), R13
        MOV 	80(R2), R14
        MOV 	88(R2), R15
	MOV	96(R2), R8		// restore R8, was saved by kernel
	MOV    	104(R2), R16		// return address from interrupt
	ADD	$112,R2,R2		// restore SP as before signal
        JMP	0(R16)			// return from interrupt (no URET on D1 chip)

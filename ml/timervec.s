	//
        // machine-mode timer interrupt.
        //
#define MRET		WORD $0x30200073

TEXT timervec(SB),0,$0
        // start.c has set up the memory that mscratch points to:
        // scratch[0,8,16] : register save area.
        // scratch[24] : address of CLINT's MTIMECMP register.
        // scratch[32] : adress of hw_timer variable
	
	CSRRW	CSR(0x0340), R8, R8	// swap mscratch
	MOV	R9,   0(R8)
	MOV	R10,  8(R8)
	MOV	R11, 16(R8)
	
        // schedule the next timer interrupt
        // by adding interval to mtimecmp, based  
	// on current value in the time CSR.
	MOV	$0x14004000,R9	// CLINT_MTIMECMP
	MOV	CSR(0x0c01),R11	// time
	MOV	32(R8), R10	// &hw_timer
	MOV	R11, 0(R10)	// time -> hw_timer var
	MOV	$300000,R10
	ADD	R10,R11,R11
	MOVW	R11,0(R9)
	SRL	$32,R11,R11
	MOVW	R11,4(R9)
	
        // raise a supervisor software interrupt.
	MOV	$2,R9
	MOVW	R9,CSR(0x0144)	// sip
	
	MOV	16(R8), R11
	MOV	 8(R8), R10
	MOV	 0(R8), R9
	CSRRW	CSR(0x0340), R8, R8	// swap mscratch

        MRET


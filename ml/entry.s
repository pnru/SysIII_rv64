// set up a stack for C.
// stack0 is declared in main.c,
// with a 4096-byte stack
// sp = stack0 + (hartid * 4096)

TEXT _entry(SB),0,$-8
	MOVW	$0,CSR(0x0304)	// = mie -> disable interrupts
	MOV	$setSB(SB),R3	// set static base

	MOV	$end(SB),R2
	ADD	$8191,R2	// set stack to end of first
	AND	$-4096,R2	// page after 'end'

	MOV	$0x20000000,R8	// maxmem
	SRA	$12,R8		// convert to clicks
	MOVW	R8,maxmem(SB)
	MOVW	R8,physmem(SB)
	
	MOV	$edata(SB),R8	// clear bss area
	MOV	$end(SB),R5
	MOVW	R0,0(R8)
	ADD	$4,R8,R8
	BLT	R5,R8,-2(PC)

	ADD	$511,R8		// _end in clicks, rounded up
	SRA	$12,R8
	MOV	R3,R8		// XXX
	JAL	R0,start(SB)

	JMP	0(PC)



extern char end[];
static char *nd = end;

int brk(char*);

char* sbrk(int incr)
{
  if(brk(nd+incr) < 0)
        return (char*)(-1);
  nd += incr;
  return nd - incr;
}

static char *env[] = { (char*)0 };

char **environ = &env[0];

int execv(char *file, char **argv)
{
	execve(file,argv,environ);
}

int execl(char *file, char *argv0, ...)
{
	execv(file, &argv0);
}


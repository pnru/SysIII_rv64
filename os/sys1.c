#include "sys/types.h"
#include "sys/param.h"
#include "sys/systm.h"
#include "sys/map.h"
#include "sys/dir.h"
#include "sys/user.h"
#include "sys/proc.h"
#include "sys/buf.h"
#include "sys/inode.h"
#include "sys/seg.h"
#include "sys/acct.h"
#include "sys/var.h"
#include "sys/tty.h"
#include "sys/page.h"
#include "sys/text.h"
#include "sys/elf.h"
#include "sys/vm.h"

/*
 * exec system call, with and without environments.
 */
struct execa {
	char	*fname;
	char	**argp;
	char	**envp;
};

exec()
{
	((struct execa *)u.u_ap)->envp = NULL;
	exece();
}

struct inode *gethead(void);
void getxelf(struct inode *ip, int nargc);

#define	NCABLK	(NCARGS+BSIZE-1)/BSIZE
extern int cnt;
exece()
{
	register unsigned nc;
	register char *cp;
	register struct buf *bp;
	register struct execa *uap;
	int na, ne, c;
	u64 ucp, ap;
	unsigned bno;
	struct inode *ip;
	extern struct inode *gethead();
	
//printf("in exec %d\n", u.u_procp->p_pid);
//memcheck("ex");
//if (cnt) printf("exec during swap\n");
	if ((ip = gethead()) == NULL)
		return;

	bp = 0;
	na = nc = ne = 0;
	uap = (struct execa *)u.u_ap;

	/* collect arglist in swap space */
	if ((bno = malloc(swapmap,NCABLK)) == 0)
		panic("Out of swap");
	if (uap->argp) for (;;) {
		ap = NULL;
		if (uap->argp) {
			ap = fuword((caddr_t)uap->argp);
			uap->argp++;
		}
		if (ap==NULL && uap->envp) {
			uap->argp = NULL;
			if ((ap = fuword((caddr_t)uap->envp)) == NULL)
				break;
			uap->envp++;
			ne++;
		}
		if (ap==NULL)
			break;
		na++;
		if (ap == -1)
			u.u_error = EFAULT;
		do {
			if (nc >= NCARGS-1)
				u.u_error = E2BIG;
			if ((c = fubyte((caddr_t)ap++)) < 0)
				u.u_error = EFAULT;
			if (u.u_error)
				goto bad;
			if ((nc&BMASK) == 0) {
				if (bp)
					bdwrite(bp);
				bp = getblk(swapdev, swplo+bno+(nc>>BSHIFT));
				cp = bp->b_un.b_addr;
			}
			nc++;
			*cp++ = c;
		} while (c>0);
	}
	if (bp)
		bdwrite(bp);
	bp = 0;
	nc = (nc + NBPW-1) & ~(NBPW-1);
	
	/* switch to new image for this process, retain u page */
	getxelf(ip, nc + sizeof(char *)*na);
	if (u.u_error) {
		psignal(u.u_procp, SIGKILL);
		goto bad;
	}

	/* copy back arglist */
	ucp = USRSTACK - nc - NBPW;
	ap = (ucp - (na+5)*sizeof(char*)) & ~0x7;
	u.u_pcb.R2 = ap+8;				/* SP */
	u.u_rval1 = na-ne;				/* argc */
	suword((caddr_t)ap, ap+2*sizeof(char*));		/* argv */ 
	ap += sizeof(char*);
	suword((caddr_t)ap, ap+(na-ne+2)*sizeof(char*)); 	/* XXX env  */

	nc = 0;
	for (;;) {
		ap += sizeof(char*);
		if (na==ne) {
			ap += sizeof(char*); /* NULL pointer at end of argv */
		}
		if (--na < 0)
			break;		
		suword((caddr_t)ap, ucp); /* pointer in argv or env */
		do {
			if ((nc&BMASK) == 0) {
				if (bp)
					brelse(bp);
				bp = bread(swapdev, swplo+bno+(nc>>BSHIFT));
				bp->b_flags |= B_AGE;
				bp->b_flags &= ~B_DELWRI;
				cp = bp->b_un.b_addr;
			}
			subyte((caddr_t)ucp++, (c = *cp++));
			nc++;
		} while (c&0377);
	}
	setregs();
	if (bp)
		brelse(bp);
	iput(ip);
	mfree(swapmap, NCABLK, bno);
//printf("pgcount = %d\n", pgcount+1);
	return;
bad:
	if (bp)
		brelse(bp);
	iput(ip);
	for (nc = 0; nc < NCABLK; nc++) {
		bp = getblk(swapdev, swplo+bno+nc);
		bp->b_flags |= B_AGE;
		bp->b_flags &= ~B_DELWRI;
		brelse(bp);
	}
	mfree(swapmap, NCABLK, bno);
}

/* Check for file existence and permissions. Return
 * inode ptr on success, NULL on error.
 */
struct inode *gethead(void)
{
	register struct inode *ip;

	if ((ip = namei(uchar, 0)) == NULL)
		return(NULL);

	if (access(ip, IEXEC) ||
	   (ip->i_mode & IFMT) != IFREG ||
	   (ip->i_mode & (IEXEC|(IEXEC>>3)|(IEXEC>>6))) == 0)
	{
		u.u_error = EACCES;
		iput(ip);
		ip = NULL;
	}
	return(ip);
}

/* Load a program segment into pagetable at virtual address va.
 * va must be page-aligned and the pages from va to va+sz must
 * already be mapped. Returns 0 on success, -1 on failure.
 */
static int
loadseg(pagetable_t pt, u64 va, struct inode *ip, u64 offset, u64 sz)
{
	u64 pa, i, n;

//printf("in loadseg\n");
	for(i = 0; i < sz; i += PGSIZE) {
		pa = walkaddr(pt, va + i);
//printf("i=%p, pa = %p, va = %p, len=%p\n", i, pa, va + i, sz);
		if(pa == 0) {
			printf("loadseg: address %p should exist", va+i);
			panic("loadseg: address %p should exist");
		}
		if(sz - i < PGSIZE)
			n = sz - i;
		else
			n = PGSIZE;
		u.u_base   = (void*)pa;
		u.u_offset = offset + i;
		u.u_count  = n;
		u.u_segflg = 1;
		readi(ip);
		u.u_segflg = 0;
		if(u.u_count != 0)
			psignal(u.u_procp, SIGTRAP);
	}
	return 0;
}

void getxelf(struct inode *ip, int nargc)
{
	register u64 i, off;
	u64 ds, ts, sz;
	struct proc *p;
	struct elfhdr  eh;
	struct proghdr ph;
	//pagetable_t pt;
	int perm;

	/* read in and check the elf header */
	p = u.u_procp;
	u.u_base = (caddr_t)&eh;
	u.u_count = sizeof(eh);
	u.u_offset = 0;
	u.u_segflg = 1;
	readi(ip);
	u.u_segflg = 0;
	if (u.u_count!=0 || eh.magic!=ELF_MAGIC) {
		u.u_error = ENOEXEC;
		return;
	}
	if ((ip->i_flag&ITEXT)==0 && ip->i_count!=1) {
		u.u_error = ETXTBSY;
		return;
	}

	/* commit to new image: release old image and reuse
	 * page table (to avoid potential OOM on free+alloc)
	 */
//printf("release ts=%p, ds=%p, ss=%p\n", u.u_tsize, u.u_dsize, u.u_ssize);
	p->p_size = p->p_ssize = 0;
	uvmunmap(p->p_pt, u.u_tsize, btoc(u.u_dsize), 1);
	uvmunmap(p->p_pt, PGROUNDDOWN(USRSTACK-u.u_ssize), btoc(u.u_ssize), 1);
	if (p->p_pid > 1)
		xfree();
	else
		uvmunmap(p->p_pt, 0LL, 1, 0);
	p->p_tsize = u.u_tsize = 0;

	u.u_pcb.epc = eh.entry;

//printf("ts=%p, ds=%p, ss=%p\n", u.u_tsize, u.u_dsize, u.u_ssize);
	/* load program segments into memory */
	u.u_tsize = u.u_dsize = u.u_ssize = 0;

	for(i=0, off=eh.phoff; i<eh.phnum; i++, off+=sizeof(ph)) {

		/* load a program header */
		u.u_base = (caddr_t)(&ph);
		u.u_count = sizeof(ph);
		u.u_offset = off;
		u.u_segflg = 1;
		readi(ip);
		u.u_segflg = 0;
		if(u.u_count != 0)
			goto bad;
		if(ph.p_type != ELF_PROG_LOAD)
			continue;
		/* sanity checks */
		if ((ph.p_memsz < ph.p_filesz) ||
		    (ph.p_vaddr + ph.p_memsz < ph.p_vaddr) ||
		    (ph.p_vaddr % PGSIZE != 0))
			goto bad;

		/* allocate & clear memory */
		if (ph.p_flags & ELF_X) {
			if (u.u_tsize)
				panic("multiple text segments");
			u.u_tsize = PGROUNDUP(ph.p_memsz);
			xalloc(ip);
			p->p_tsize = btoc(u.u_tsize);
			if (p->p_textp->x_flag&XLOAD) {
				loadseg(p->p_pt, ph.p_vaddr, ip, ph.p_off, ph.p_filesz);
				p->p_textp->x_flag = XWRIT;
			}
		}
		else {
			expand(ph.p_memsz, SEG_DATA);
			imgclr(u.u_tsize, u.u_tsize+u.u_dsize);
//printf("memsz=%p, p_size=%x\n", ph.p_memsz, p->p_size);
			loadseg(p->p_pt, ph.p_vaddr, ip, ph.p_off, ph.p_filesz);
		}
	}
	expand(SSIZE+(NCARGS-1), SEG_STACK);
	chksize(u.u_tsize, u.u_dsize, u.u_ssize);	
//printf("past load\n");
//uvmlist(pt, 0, 12);
//printf("exec %d\n", p->p_pid);
//ptaudit(p, "exec");

	/* set SUID/SGID protections, if no tracing */
	if ((u.u_procp->p_flag&STRC)==0) {
		if (ip->i_mode&ISUID)
			u.u_uid = ip->i_uid;
		if (ip->i_mode&ISGID)
			u.u_gid = ip->i_gid;
	} else
		psignal(u.u_procp, SIGTRAP);
//printf("exec end\n");
//w_trace(1);
	return;

bad:
	u.u_error = EFAULT;
	return;
}

/* Clear selected state on exec */
setregs()
{
	register int *rp;
	register char *cp;
	register i;

	/* reset ignored signals */
	for (rp = &u.u_signal[0]; rp < &u.u_signal[NSIG]; rp++)
		if ((*rp & 1) == 0)
			*rp = 0;

	/* close files on exec as specified */
	for (i=0; i<NOFILE; i++) {
		if ((u.u_pofile[i]&EXCLOSE) && u.u_ofile[i] != NULL) {
			closef(u.u_ofile[i]);
			u.u_ofile[i] = NULL;
		}
	}

	/* Remember file name for accounting */
	u.u_acflag &= ~AFORK;
	bcopy((caddr_t)u.u_dent.d_name, (caddr_t)u.u_comm, DIRSIZ);
}

/*
 * exit system call:
 * pass back caller's arg
 */
rexit()
{
	register struct a {
		u64	rval;
	} *uap;
//printf("in exit\n");
	uap = (struct a *)u.u_ap;
	exit((int)(uap->rval & 0377) << 8);
}

/*
 * Release resources.
 * Enter zombie state.
 * Wake up parent and init processes,
 * and dispose of children.
 */
exit(rv)
{
	register int i;
	int flg = 0;
	register struct proc *p, *q;

	p = u.u_procp;
	p->p_flag &= ~(STRC);
	p->p_clktim = 0;
	for (i=0; i<NSIG; i++)
		u.u_signal[i] = 1;
	if ((p->p_pid == p->p_pgrp)
	 && (u.u_ttyp != NULL)
	 && (u.u_ttyp->t_pgrp == p->p_pgrp)) {
		u.u_ttyp->t_pgrp = 0;
		signal(p->p_pgrp, SIGHUP);
	}
	p->p_pgrp = 0;
	for (i=0; i<NOFILE; i++) {
		if (u.u_ofile[i] != NULL)
			closef(u.u_ofile[i]);
	}
	plock(u.u_cdir);
	iput(u.u_cdir);
	if (u.u_rdir) {
		plock(u.u_rdir);
		iput(u.u_rdir);
	}
//if (p->p_textp->x_ccount==1) flg = 1;
	xfree();
//	acct(rv);
	imgzap(p);
	p->p_size = p->p_ssize = p->p_swsize = 0;
//printf("past exitzap %d\n", p->p_pid);

	p->p_stat = SZOMB;
	((struct xproc *)p)->xp_xstat = rv;
	((struct xproc *)p)->xp_utime = u.u_cutime + u.u_utime;
	((struct xproc *)p)->xp_stime = u.u_cstime + u.u_stime;
	for (q = &proc[1]; q < (struct proc *)v.ve_proc; q++) {
		if (p->p_pid == q->p_ppid) {
			q->p_ppid = 1;
			if (q->p_stat == SZOMB)
				psignal(&proc[1], SIGCLD);
			if (q->p_stat == SSTOP)
				setrun(q);
		} else
		if (p->p_ppid == q->p_pid)
			psignal(q, SIGCLD);
		if (p->p_pid == q->p_pgrp)
			q->p_pgrp = 0;
	}
//printf("pgcount = %d\n", pgcount+1);
	/* cannot free u-page yet, in use for stack, deferred to freeproc() */
//if (p->p_pid>4 && flg) w_trace(1);
	resume(proc[0].p_upa, u.u_qsav);
	/* no deposit, no return */
}

/*
 * Wait system call.
 * Search for a terminated (zombie) child,
 * finally lay it to rest, and collect its status.
 * Look also for stopped (traced) children,
 * and pass back status from them.
 */
wait()
{
	register f;
	register struct proc *p;

loop:
	f = 0;
	for (p = &proc[1]; p < (struct proc *)v.ve_proc; p++)
	if (p->p_ppid == u.u_procp->p_pid) {
		f++;
		if (p->p_stat == SZOMB) {
			freeproc(p, 1);
			return;
		}
		if (p->p_stat == SSTOP) {
			if ((p->p_flag&SWTED) == 0) {
				p->p_flag |= SWTED;
				u.u_rval1 = p->p_pid;
				u.u_rval2 = (fsig(p)<<8) | 0177;
				return;
			}
			continue;
		}
	}
	if (f) {
		sleep((caddr_t)u.u_procp, PWAIT);
		goto loop;
	}
	u.u_error = ECHILD;
}

/*
 * Remove zombie children from the process table.
 */
freeproc(p, flag)
register struct proc *p;
{
	if (flag) {
		u.u_rval1 = p->p_pid;
		u.u_rval2 = ((struct xproc *)p)->xp_xstat;
	}
	u.u_cutime += ((struct xproc *)p)->xp_utime;
	u.u_cstime += ((struct xproc *)p)->xp_stime;
	p->p_stat = NULL;
	p->p_pid = 0;
	p->p_ppid = 0;
	p->p_sig = 0L;
	p->p_flag = 0;
	p->p_wchan = 0;
	p->p_swsize = 0;
	kfree(p->p_upa);
}

/*
 * fork system call.
 */
fork()
{
	register n;
	register struct proc *p1, *p2;
	register a;

	/*
	 * Make sure there's enough swap space for max
	 * core image, thus reducing chances of running out
	 */
	if ((a = malloc(swapmap, ctod(MAXMEM))) == 0) {
		u.u_error = ENOMEM;
		goto out;
	}
	mfree(swapmap, ctod(MAXMEM), a);
	a = 0;
	p1 = &proc[0];
	p2 = NULL;
	n = v.v_proc;
	do {
		if (p1->p_stat==NULL && p2==NULL)
			p2 = p1;
		else {
			if (p1->p_uid==u.u_uid && p1->p_stat!=NULL)
				a++;
		}
		p1++;
	} while (--n);
	/*
	 * Disallow if
	 *  No processes at all;
	 *  not su and too many procs owned; or
	 *  not su and would take last slot.
	 */
	if (p2==NULL || (u.u_uid!=0 && (p2==&proc[v.v_proc-1] || a>v.v_maxup))) {
		u.u_error = EAGAIN;
		goto out;
	}
	if (newproc()) {
		u.u_rval1 = 0; //XXX p2->p_ppid;
		u.u_rval2 = 1;  /* child */
		u.u_start = time;
		u.u_ticks = lbolt;
		u.u_mem = p2->p_size;
		u.u_ior = u.u_iow = u.u_ioch = 0;
		u.u_cstime = 0;
		u.u_stime = 0;
		u.u_cutime = 0;
		u.u_utime = 0;
		u.u_acflag = AFORK;
		return;
	}
	u.u_rval1 = p2->p_pid;

out:
	u.u_rval2 = 0;
}

/*
 * break system call.
 *  -- bad planning: "break" is a dirty word in C.
 */
sbreak()
{
	struct a {
		u64 nsiz;
	};
	register u64 n, b;
	register i64 d;
	register struct pt_entry *ptaddr;

	/*
	 * set n to new data size
	 * set d to new-old
	 */

	b = PGROUNDUP(u.u_dsize) + u.u_tsize;
	n = PGROUNDUP(((struct a *)u.u_ap)->nsiz);

	d = n - b;
	if (d > 0 && chksize(u.u_tsize, n, u.u_ssize))
		return;
//printf("ns=%p, b=%p\n", ((struct a *)u.u_ap)->nsiz, b);
	expand(d, SEG_DATA);
	if (d>0)
		imgclr(b, n);
//ptaudit(u.u_procp, "sbreak");
}

#include "sys/types.h"

struct map
{
	short	m_size;
	unsigned short m_addr;
};

extern struct map swapmap[];

int   malloc(struct map *mp, int size);
int   mfree(struct map *mp, int size, unsigned int a);

void *kalloc(void);
void  kfree(void *pa);
void  freerange(void *pa_start, void *pa_end);
u64   meminit(void);

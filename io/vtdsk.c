/*
 * VIRTIO disk driver for TinyEMU disk device
 */

#include "sys/types.h"
#include "sys/param.h"
#include "sys/dir.h"
#include "sys/user.h"
#include "sys/buf.h"
#include "sys/elog.h"
#include "sys/iobuf.h"
#include "sys/systm.h"
#include "sys/vm.h"

#include "virtio.h"

struct	iobuf	vttab;

// the address of virtio mmio register r.
// virtio mmio interface
#define R(r)		((u32 *)(VIRTIO0 + (r)))

/* 3 arrays with NUM descriptors */
static struct virtq_desc  desc[NUM];	/* descriptor pool     */
static struct virtq_avail avail[NUM];	/* driver request ring */
static struct virtq_used  used[NUM];	/* device rsponse ring */

static char	free[NUM]; 	/* is descriptor free? */
static u16	seen_idx;	/* last response seen */

/* track info about in-flight operations, for use when completion interrupt
 * arrives; indexed by first descriptor index of chain.
 */
struct {
	struct buf	*bp;
	char		status;
} info[NUM];

/* disk command headers. one-for-one with descriptors, for convenience. */
struct virtio_blk_req	ops[NUM];

vtinit()
{
	u32 status = 0;
	u32 max;
	u64 features;

	if (*R(VIRTIO_MMIO_MAGIC_VALUE) != 0x74726976 ||
	    *R(VIRTIO_MMIO_VERSION)     != 2 ||
	    *R(VIRTIO_MMIO_DEVICE_ID)   != 2 ||
	    *R(VIRTIO_MMIO_VENDOR_ID)   != 0xffff) {
		panic("could not find virtio disk");
	}

	/* reset device */
	*R(VIRTIO_MMIO_STATUS) = status;
	
	/* set ACKNOWLEDGE status bit */
	status |= VIRTIO_CONFIG_S_ACKNOWLEDGE;
	*R(VIRTIO_MMIO_STATUS) = status;
	
	/* set DRIVER status bit */
	status |= VIRTIO_CONFIG_S_DRIVER;
	*R(VIRTIO_MMIO_STATUS) = status;
	
	/* negotiate features */
	features = *R(VIRTIO_MMIO_DEVICE_FEATURES);
	features &= ~(1 << VIRTIO_BLK_F_RO);
	features &= ~(1 << VIRTIO_BLK_F_SCSI);
	features &= ~(1 << VIRTIO_BLK_F_CONFIG_WCE);
	features &= ~(1 << VIRTIO_BLK_F_MQ);
	features &= ~(1 << VIRTIO_F_ANY_LAYOUT);
	features &= ~(1 << VIRTIO_RING_F_EVENT_IDX);
	features &= ~(1 << VIRTIO_RING_F_INDIRECT_DESC);
	*R(VIRTIO_MMIO_DRIVER_FEATURES) = features;
	
	/* tell device that feature negotiation is complete. */
	status |= VIRTIO_CONFIG_S_FEATURES_OK;
	*R(VIRTIO_MMIO_STATUS) = status;
	
	/* re-read status to ensure FEATURES_OK is set. */
	status = *R(VIRTIO_MMIO_STATUS);
	if(!(status & VIRTIO_CONFIG_S_FEATURES_OK))
		panic("virtio disk FEATURES_OK unset");
	
	/* initialize queue 0. */
	*R(VIRTIO_MMIO_QUEUE_SEL) = 0;
	
	/* ensure queue 0 is not in use. */
	if(*R(VIRTIO_MMIO_QUEUE_READY))
		panic("virtio disk should not be ready");
	
	/* check maximum queue size. */
	max = *R(VIRTIO_MMIO_QUEUE_NUM_MAX);
	if(max == 0)
		panic("virtio disk has no queue 0");
	if(max < NUM)
		panic("virtio disk max queue too short");
	
	/* set queue size. */
	*R(VIRTIO_MMIO_QUEUE_NUM) = NUM;
	
	/* write physical addresses. */
	*R(VIRTIO_MMIO_QUEUE_DESC_LOW)   = (u64)desc;
	*R(VIRTIO_MMIO_QUEUE_DESC_HIGH)  = (u64)desc >> 32;
	*R(VIRTIO_MMIO_DRIVER_DESC_LOW)  = (u64)avail;
	*R(VIRTIO_MMIO_DRIVER_DESC_HIGH) = (u64)avail >> 32;
	*R(VIRTIO_MMIO_DEVICE_DESC_LOW)  = (u64)used;
	*R(VIRTIO_MMIO_DEVICE_DESC_HIGH) = (u64)used >> 32;
	
	/* queue is ready. */
	*R(VIRTIO_MMIO_QUEUE_READY) = 0x1;
	
	/* all NUM descriptors start out unused. */
	for(int i = 0; i < NUM; i++)
		free[i] = 1;
	
	/* tell device we're completely ready. */
	status |= VIRTIO_CONFIG_S_DRIVER_OK;
	*R(VIRTIO_MMIO_STATUS) = status;
}

/* find a free descriptor, mark it non-free, return its index. */
static int
alloc_desc()
{
	for(int i = 0; i < NUM; i++){
		if(free[i]){
			free[i] = 0;
			return i;
		}
	}
	return -1;
}

/* mark a descriptor as free. */
static void
free_desc(int i)
{
	if(i >= NUM)
		panic("free_desc 1");
	if(free[i])
		panic("free_desc 2");
	desc[i].addr  = 0;
	desc[i].len   = 0;
	desc[i].flags = 0;
	desc[i].next  = 0;
	free[i] = 1;
	wakeup(&free[0]);
}

/* free a chain of descriptors. */
static void
free_chain(int i)
{
	while(1){
		int flag = desc[i].flags;
		int nxt  = desc[i].next;
		free_desc(i);
		if(flag & VRING_DESC_F_NEXT)
			i = nxt;
		else
			break;
	}
}

/* allocate three descriptors (they need not be contiguous).
 * disk transfers always use three descriptors.
 */
static int
alloc3_desc(int *idx)
{
	for(int i = 0; i < 3; i++){
		idx[i] = alloc_desc();
		if(idx[i] < 0){
			for(int j = 0; j < i; j++)
				free_desc(idx[j]);
			return -1;
		}
	}
	return 0;
}

/* Initiate request to device. Must be called with interrupts disabled.
 */
void
virtio_disk_rw(struct buf *bp)
{
	int lck, read = bp->b_flags & B_READ;
	u64 sector = bp->b_blkno * (BSIZE / 512);
	struct virtio_blk_req *req;
	int chn[3];

	while(1){
		if (alloc3_desc(chn) == 0) {
			break;
		}
		sleep(&free[0], PRIBIO+1);
	}

	/* three descriptors: one for type/reserved/sector, one for the
	 * data, one for a 1-byte status result.
	 */
	req = &ops[chn[0]];
	req->type     = (read) ? VIRTIO_BLK_T_IN : VIRTIO_BLK_T_OUT;
	req->reserved = 0;
	req->sector   = sector;

	desc[chn[0]].addr   = (u64) req;
	desc[chn[0]].len    = sizeof(struct virtio_blk_req);
	desc[chn[0]].flags  = VRING_DESC_F_NEXT;
	desc[chn[0]].next   = chn[1];

	desc[chn[1]].addr   = (u64) bp->b_un.b_addr;
	desc[chn[1]].len    = bp->b_bcount;
	desc[chn[1]].flags  = read ? VRING_DESC_F_WRITE : 0; /* dev writes */
	desc[chn[1]].flags |= VRING_DESC_F_NEXT;
	desc[chn[1]].next   = chn[2];

	info[chn[0]].status = 0xff; /* device writes 0 on success */
	desc[chn[2]].addr   = (u64) &info[chn[0]].status;
	desc[chn[2]].len    = 1;
	desc[chn[2]].flags  = VRING_DESC_F_WRITE;
	desc[chn[2]].next   = 0;

	info[chn[0]].bp = bp;

	/* tell the device the first index in our chain of descriptors and
	 * tell the device another avail ring entry is available.
	 */
	avail->ring[avail->idx % NUM] = chn[0];
	avail->idx++; 

	*R(VIRTIO_MMIO_QUEUE_NOTIFY) = 0; /* value is queue number */
}

vtopen(int dev)
{
}

vtclose(int dev)
{
}

vtstrategy(struct buf *bp)
{
	int lck;

	if ( (bp->b_blkno > 20000) || (bp->b_blkno < 0) )
	{
		bp->b_flags |= B_ERROR;
		iodone(bp);
		return;
	}

	bp->av_forw = 0;

	/* link buffer to end of service queue */
	lck = spl7();
	
	if (vttab.b_actf == 0)
		vttab.b_actf = bp;
	else
		vttab.b_actl->av_forw = bp;
	vttab.b_actl = bp;
	/* fdtab.b_active is always 0 for now */
	if (vttab.b_active == 0)
		vtstart();

	splx(lck);
}

vtstart(void)
{
	register struct buf *bp;

	/* Unlink immediately; there is always 0 or 1 request in the queue */
	if ((bp = vttab.b_actf) == 0)
		return;
	vttab.b_actf = bp->av_forw;
	
	virtio_disk_rw(bp);
}

void
vtintr(void)
{
	struct buf *bp;
	int lck, id;

	/* may race on used->idx, but harmless: we simply work ahead */
	*R(VIRTIO_MMIO_INTERRUPT_ACK) = *R(VIRTIO_MMIO_INTERRUPT_STATUS) & 0x3;

	/* the device increments used->idx when it adds an entry
	   to the used ring. */
	while(seen_idx != used->idx){
		id = used->ring[seen_idx % NUM].id;
		bp = info[id].bp;
		if(info[id].status != 0)
			bp->b_flags |= B_ERROR;

		info[id].bp = 0;
		free_chain(id);
		iodone(bp);
		seen_idx += 1;
	}
}

struct buf vtbuf;

vtread(int dev)
{
		physio(vtstrategy, &vtbuf, dev, B_READ);
}

vtwrite(int dev)
{
		physio(vtstrategy, &vtbuf, dev, B_WRITE);
}


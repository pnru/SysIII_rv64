/*
 * fundamental variables
 * don't change too often
 */

#define	NOFILE	20		/* max open files per process */
#define	MAXPID	30000		/* max process id */
#define	MAXUID	60000		/* max user id */
#define	MAXLINK	1000		/* max links */

#define	MAXMEM	(768)		/* max core in 4096-byte clicks */
/*
 * NOTE: For the moment, MAXUMEM must be less than 60*128
 */
#define	MAXUMEM	(40*128)	/* max no. clicks per process */
#define	SWAPSIZE	1	/* granularity of partial swaps (in clicks) */
#define	SSIZE	2048		/* initial stack size (in bytes) */
#define	SINCR	1024		/* increment of stack (in bytes) */
#define	UPAGES	1		/* one 4KB page for struct user and system stack */
#define	USRSTACK 0x80000000UL	/* Start of user stack */

#define	CANBSIZ	256		/* max size of typewriter line	*/
#define	HZ	60		/* Ticks/second of the clock */
#define	NCARGS	5120		/* # characters in exec arglist */

/*
 * priorities
 * probably should not be
 * altered too much
 */

#define	PSWP	0
#define	PINOD	10
#define	PRIBIO	20
#define	PZERO	25
#define	NZERO	20
#define	PPIPE	26
#define	PWAIT	30
#define	PSLEP	40
#define	PUSER	50
#define	PIDLE	127

/*
 * signals
 * dont change
 */

#define	NSIG	20
/*
 * No more than 32 signals (1-32) because they are
 * stored in bits in a long.
 */
#define	SIGHUP	1	/* hangup */
#define	SIGINT	2	/* interrupt (rubout) */
#define	SIGQUIT	3	/* quit (FS) */
#define	SIGILL	4	/* illegal instruction */
#define	SIGTRAP	5	/* trace or breakpoint */
#define	SIGIOT	6	/* iot */
#define	SIGEMT	7	/* emt */
#define	SIGFPE	8	/* floating exception */
#define	SIGKILL	9	/* kill, uncatchable termination */
#define	SIGBUS	10	/* bus error */
#define	SIGSEGV	11	/* segmentation violation */
#define	SIGSYS	12	/* bad system call */
#define	SIGPIPE	13	/* end of pipe */
#define	SIGALRM	14	/* alarm clock */
#define	SIGTERM	15	/* Catchable termination */
#define	SIGUSR1	16	/* user defined signal 1 */
#define	SIGUSR2	17	/* user defined signal 2 */
#define	SIGCLD	18	/* child death */
#define	SIGPWR	19	/* power-fail restart */

/*
 * fundamental constants of the implementation--
 * cannot be changed easily
 */

#define	NBPW	sizeof(int)	/* number of bytes in an integer */
#define	BSIZE	512		/* size of secondary block (bytes) */
#define	NINDIR	(BSIZE/sizeof(daddr_t))
#define	BMASK	0777		/* BSIZE-1 */
#define	INOPB	8		/* inodes per block */
#define	BSHIFT	9		/* LOG2(BSIZE) */
#define	NMASK	0177		/* NINDIR-1 */
#define	NSHIFT	7		/* LOG2(NINDIR) */
#define	NICFREE	50		/* number of superblock free blocks */
#undef NULL
#define	NULL	0
#define	CMASK	0		/* default mask for file creation */
#define	CDLIMIT	(1L<<11)	/* default max write address */
#define	NODEV	(dev_t)(-1)
#define	ROOTINO	((ino_t)1)	/* i number of all roots */
#define	SUPERB	((daddr_t)1)	/* block number of the super block */
#define	DIRSIZ	14		/* max characters per directory */
#define	NICINOD	100		/* number of superblock inodes */
#define	CLKTICK	16667		/* microseconds in a  clock tick */

/*
 * Some macros for units conversion
 */
/* Core clicks (512/64 bytes) to segments and vice versa */
#ifdef vax
#define	ctos(x)	(x)
#define	stoc(x)	(x)
#else
#define	ctos(x)	((x+127)/128)
#define	stoc(x)	((x)<<7)
#endif

/* Core clicks (512/64 bytes) to disk blocks */
#ifdef vax
#define	ctod(x)	((x)<<3)
#else
#define	ctod(x)	((x+  7)>>3)
#endif

/* inumber to disk address */
#define	itod(x)	(daddr_t)((((unsigned)x+15)>>3))

/* inumber to disk offset */
#define	itoo(x)	(int)((x+15)&07)

/* clicks to bytes */
#define	ctob(x)	(((u64)x)<<12)

/* bytes to clicks */
#define	btoc(x)		(((u64)x+4095)>>12)
#define	btoct(x)	((u64)(x)>>12)

/* major part of a device */
#define	major(x)	(int)((unsigned)x>>8)

/* minor part of a device */
#define	minor(x)	(int)(x&0377)

/* make a device number */
#define	makedev(x,y)	(dev_t)(((x)<<8) | (y))

typedef	struct { int r[1]; } *	physadr;

/* named types, duplicate from types.h XXX */
typedef	long		daddr_t;
typedef	char *		caddr_t;
typedef	unsigned short	ushort;
typedef	ushort		ino_t;
typedef short		cnt_t;
typedef	long		time_t;
typedef	long		label_t[14];
typedef	short		dev_t;
#ifndef __APPLE__
typedef	long		off_t;
#endif
typedef	long long	paddr_t; /* long for 32 bit RISCV */

#define	UMODE	PS_CUR		/* usermode bits */
//#define	USERMODE(ps)	((ps & UMODE) == UMODE)

#define SSTATUS_SPP	(1L << 8)	// Previous mode, 1=Supervisor, 0=User
#define USERMODE(sstatus)  ((sstatus & SSTATUS_SPP) == 0)

//#define	BASEPRI(ps)	((ps & PS_IPL) != 0)
#define SSTATUS_SIE (1L << 1)
#define	BASEPRI(sstatus)	((sstatus & SSTATUS_SIE) != 0)

#define	lobyte(X)	(((unsigned char *)&X)[0])
#define	hibyte(X)	(((unsigned char *)&X)[1])
#define	loword(X)	(((ushort *)&X)[0])
#define	hiword(X)	(((ushort *)&X)[1])


#include "sys/param.h"
#include "sys/var.h"
#include "sys/proc.h"
#include "sys/text.h"
#include "sys/dir.h"
#include "sys/user.h"

struct proc pline;
struct var v;
struct text tline;
struct user up;

main()
{
	int vf, kf, flg, wchan, i;
	struct proc *p = &pline;
	struct text *xp = &tline;
	long offs;
	
	// get kernel var struct
	vf = open("/dev/proc", 0);
	read(vf, &v, sizeof(struct var));
	close(vf);

	while(1) {
		kf = open("/dev/kmem", 0);

		offs = (long)(v.vb_proc);
		printf("\nPID STATE FLAGS PPID WCHAN SIZE SSZE TEXT SWSZ   UPA  PROC  TEXT  CMD\n");
		for (i = 0; i < v.v_proc; i++, offs += sizeof(*p)) {
			lseek(kf, offs, 0);
			read(kf, p, sizeof(*p));
			if (p->p_stat == 0) continue;
			flg = p->p_flag & 0xff;
			wchan = (int)p->p_wchan & 0xfffff;
			printf("%3d %5d %5o %4d %05x %4d %4d %4d %4d %05x %05x %05x",
				p->p_pid, p->p_stat, flg, p->p_ppid, wchan, p->p_size, p->p_ssize, p->p_tsize,
				p->p_swsize, ((long)p->p_upa)>>12, offs&0xfffff, (long)p->p_textp&0xfffff);
			if (p->p_textp) {
				lseek(kf, (long)(p->p_textp), 0);
				read(kf, xp, sizeof(*xp));
				printf("%c", (offs == (long)(xp->x_procp)) ? '*' : ' ');
			}
			else
				printf(" ");
			lseek(kf, (long)p->p_upa, 0);
			read(kf, &up, sizeof(up));
			if (p->p_pid==0) strcpy(up.u_comm, "[scheduler]");
			printf(" %s\n", up.u_comm);
		}
		
		offs = ((long)(v.ve_text)) - v.v_text * sizeof(*xp);
		printf("\nIDX  PROC  IPTR CNT CCNT FLAGS\n");
		for (i = 0; i < v.v_text; i++, offs += sizeof(*xp)) {
			lseek(kf, offs, 0);
			read(kf, xp, sizeof(*xp));
			if (xp->x_iptr==0) continue;
			printf("%3d %05x %05x %3d %4d %5o\n", i, (long)xp->x_procp&0xfffff, (long)xp->x_iptr&0xfffff,
				xp->x_count, xp->x_ccount, xp->x_flag);
		}
		printf("\n");
			
		close(kf);
		//sleep(1);
		break;
	}
}

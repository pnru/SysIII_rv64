
#define PGSIZE		4096 // bytes per page
#define PGSHIFT		12  // bits of offset within a page
#define PGOFFS(a)	((a) & (PGSIZE-1))

#define PGROUNDUP(sz)  (((sz)+PGSIZE-1) & ~(PGSIZE-1))
#define PGROUNDDOWN(a) (((a)) & ~(PGSIZE-1))

#define PTE_V (1L << 0) // valid
#define PTE_R (1L << 1)
#define PTE_W (1L << 2)
#define PTE_X (1L << 3)
#define PTE_U (1L << 4) // 1 -> user can access
#define PTE_A (1L << 6)
#define PTE_D (1L << 7)
#define PTE_L (1L << 8) // 1 -> pte does not own page


#define SATP_SV39 (8LL << 60)
#define MAKE_SATP(pagetable) (SATP_SV39 | (((u64)pagetable) >> 12))

// shift a physical address to the right place for a PTE.
#define PA2PTE(pa)	((((u64)pa) >> 12) << 10)
#define PTE2PA(pte)	(((pte) >> 10) << 12)
#define PTE_FLAGS(pte)	((pte) & 0x3FF)

// extract the three 9-bit page table indices from a virtual address.
#define PXMASK          0x1FF // 9 bits
#define PXSHIFT(level)  (PGSHIFT+(9*(level)))
#define PX(level, va) ((((u64) (va)) >> PXSHIFT(level)) & PXMASK)

/* one beyond the highest possible virtual address.
 * MAXVA is actually one bit less than the max allowed by
 * Sv39, to avoid having to sign-extend virtual addresses
 * that have the high bit set.
 */
#define MAXVA (1ULL << (9 + 9 + 9 + 12 - 1))

#define TRAMPOLINE (MAXVA - PGSIZE)

#define UART0		0x02500000
#define PLIC		0x10000000
#define VIRTIO0		0x11001000
#define KERNBASE	0x40000000
#define PHYSTOP		0x40400000

#define SEG_TEXT	1
#define SEG_DATA	2
#define SEG_STACK	3

typedef u64  pte_t;
typedef u64 *pagetable_t; // 512 PTEs

extern pagetable_t kernel_pagetable;
extern pte_t *u_pte;
extern void* trampo; // phys addr of trampoline page
extern unsigned pgcount;

void  sfence_vma(void);
void* kalloc(void);
void  kfree(void *pa);
int   chksize(u64 text, u64 data, u64 stack);

void  kvminit(void);
void  kvm_start(void);
void  kvmmap(pagetable_t kpgtbl, u64 va, u64 pa, u64 sz, int perm);
int   mappages(pagetable_t pt, u64 va, u64 size, u64 pa, int perm);

u64   uvmalloc(pagetable_t pt, u64 va_low, u64 va_high, int perm);
int   uvmcopy(pagetable_t old, pagetable_t new, u64 va, u64 sz, int ref);
void  uvmunmap(pagetable_t pagetable, u64 va, u64 npages, int do_free);
u64   uvmdealloc(pagetable_t pagetable, u64 oldsz, u64 newsz, int do_free);
void  uvmzap(pagetable_t pt);
void  uvmfree(pagetable_t pagetable, u64 sz);
pagetable_t new_pt(struct user *old_u);
void  expand(i64 change, int region);
void  imgzap(struct proc *p);
void  testwalk(pagetable_t pagetable, u64 va);
u64   walkaddr(pagetable_t pagetable, u64 va);
uvmlist(pagetable_t pagetable, u64 va, u64 npages);
pte_t *walk(pagetable_t pagetable, u64 va, int alloc);

int  copyout(char *src, u64 dstva, u32 len);
int  copyin(u64 srcva, char *dst, u32 len);
int  copyinstr(pagetable_t pagetable, char *dst, u64 srcva, u64 max);
int  fubyte(void *addr);
int  subyte(void *addr, u8 val);
u64  fuptr(void *addr);
int  suptr(void *addr, u64 val);
u64  fuword(void *addr);
int  suword(void *addr, u64 val);

void swap(struct proc *p, int blkno, int addr, int count, int rdflag);


void *memcpy(dst, src, size)
void* dst;
void* src;
unsigned size;
{
  register char *d;
  register char *s;
  register unsigned n;

  if (size <= 0) return(dst);

  s = (char *) src;
  d = (char *) dst;
  if (s <= d && s + (size - 1) >= d) {
	/* Overlap, must copy right-to-left. */
	s += size - 1;
	d += size - 1;
	for (n = size; n > 0; n--) *d-- = *s--;
  } else
	for (n = size; n > 0; n--) *d++ = *s++;

  return(dst);
}

void *memmove(dst, src, size)
void* dst;
void* src;
unsigned size;
{
	memcpy(dst, src, size);
}


#include "sys/types.h"
#ifndef TTY_H
#include "sys/tty.h"
#endif

/*
 * Declaration of block device switch. Each entry (row) is
 * the only link between the main unix code and the driver.
 * The initialization of the device switches is in the file conf.c.
 */
extern struct bdevsw
{
	int	(*d_open)();
	int	(*d_close)();
	int	(*d_strategy)();
	struct iobuf *d_tab;
} bdevsw[];

/*
 * Character device switch.
 */
extern struct cdevsw
{
	int	(*d_open)();
	int	(*d_close)();
	int	(*d_read)();
	int	(*d_write)();
	int	(*d_ioctl)(int, int, void *, int);
} cdevsw[];

int	bdevcnt;
int	cdevcnt;

/*
 * Line discipline switch.
 */

extern struct linesw
{
	int	(*l_open)();
	int	(*l_close)();
	int	(*l_read)();
	int	(*l_write)();
	int	(*l_ioctl)(struct tty *, int, void *, int);
	int	(*l_input)(struct tty *, ucp_t, int);
	int	(*l_output)(struct tty *, ucp_t, int);
	int	(*l_mdmint)();
} linesw[];

int	linecnt;

# setjmp, longjmp
#
#	longjmp(a, v)
# causes a "return(v)" from the
# last call to
#
#	setjmp(v)
# by restoring all the registers and
# adjusting the stack
#
# jmp_buf is set up as:
#
#	_________________
#	|	pc	|
#	-----------------
#	|	d2	|
#	-----------------
#	|	...	|
#	-----------------
#	|	d7	|
#	-----------------
#	|	a2	|
#	-----------------
#	|	...	|
#	-----------------
#	|	a7	|
#	-----------------

	global setjmp
	text
setjmp:
	mov.l	4(%sp),%a0	# pointer to jmp_buf
	mov.l	(%sp),(%a0)	# save pc
	movm.l	&0xfcfc,4(%a0)  # save d2-d7, a2-a7
	mov.l	&0,%d0		# return 0
	rts

	global longjmp
	text
longjmp:
	mov.l	4(%sp),%a0	# pointer to jmp_buf
	mov.l	8(%sp),%d0	# value returned
	bne	L%01		# force d0 to be non zero
	mov.l	&1,%d0		# force d0 to be non zero
L%01:	movm.l	4(%a0),&0xfcfc	# restore d2-d7, a2-a7
	mov.l	(%a0),(%sp)	# restore pc of call to setjmp to stack
	rts

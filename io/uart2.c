/*
 *   16650 uart driver
 */
#include "sys/types.h"
#include "sys/param.h"
#include "sys/dir.h"
#include "sys/file.h"
#include "sys/tty.h"
#include "sys/conf.h"
#include "sys/user.h"

// UART addresses on D1 chip
#define UART_BASE	(0x02500000)
#define UART_SZ		(0x4000)

#define UART0		0

// Type 16650 uart registers
typedef struct
{
	u32 rbr;		/* 0 */
	u32 ier;		/* 1 */
	u32 fcr;		/* 2 */
	u32 lcr;		/* 3 */
	u32 mcr;		/* 4 */
	u32 lsr;		/* 5 */
	u32 msr;		/* 6 */
	u32 sch;		/* 7 */
} uart_t;

#define thr rbr
#define dll rbr
#define dlh ier
#define iir fcr

#define THRE	0x20    /* transmit-hold-register empty */
#define DR	0x01    /* receiver data ready */

#define RIE	0x01	/* receive  interrupt enable */
#define XIE	0x02	/* transmit interrupt enable */

static uart_t *addr = (uart_t *)(UART_BASE + UART0 * UART_SZ);

struct tty ua_tty[1];
extern int ua_cnt;

uaproc(struct tty *tp, int cmd);

uaopen(dev, flag)
{
	register struct tty *tp;
	extern dlproc();

	if(dev >= ua_cnt) {
		u.u_error = ENXIO;
		return;
	}
	tp = &ua_tty[dev];
	if ((tp->t_state&(ISOPEN|WOPEN)) == 0) {
		ttinit(tp);
		tp->t_proc = uaproc;
	}
	spl7();
	tp->t_state |= CARR_ON;
	if (!(flag&FNDELAY))
		while ((tp->t_state&CARR_ON)==0) {
			tp->t_state |= WOPEN;
			sleep((caddr_t)&tp->t_canq, TTIPRI);
		}
	(*linesw[tp->t_line].l_open)(tp);
	addr->ier = RIE|XIE;
	addr->fcr  = 0x00;
	addr->mcr  = 0x00;
	spl0();
}

uaclose(dev)
{
	register struct tty *tp;

	tp = &ua_tty[dev];
	addr->ier &= ~(RIE|XIE);
	(*linesw[tp->t_line].l_close)(tp);
}

uaread(dev)
{
	register struct tty *tp;

	tp = &ua_tty[dev];
	(*linesw[tp->t_line].l_read)(tp);
}

uawrite(dev)
{
	register struct tty *tp;

	tp = &ua_tty[dev];
	(*linesw[tp->t_line].l_write)(tp);
}

// Poll-wait UART0 routines
//
void putchar(char c)
{
	while((addr->lsr & THRE) == 0);
	addr->thr = c;
}

uaxint(dev)
{
	register struct tty *tp;

	tp = &ua_tty[dev];
	addr->ier &= ~XIE;
	if (addr->lsr&THRE) {
		//addr->ier &= ~XIE;
		tp->t_state &= ~BUSY;
		if (tp->t_state & TTXON) {
			addr->thr = CSTART;
			tp->t_state &= ~TTXON;
		} else if (tp->t_state & TTXOFF) {
			addr->thr = CSTOP;
			tp->t_state &= ~TTXOFF;
		} else
			uaproc(tp, T_OUTPUT);
	}
}

uarint(dev)
{
	register int c;
	register struct tty *tp;
	register ucp_t ucp;

	tp = &ua_tty[dev];
//printf("rd int\n");
	while(addr->lsr & DR) {
		ucp.ch = addr->rbr;
//printf("dev=%d, c=%x\n", dev, ucp.ch);
		(*linesw[tp->t_line].l_input)(tp, ucp, 0);
	}
}

uaint(dev)
{
	u32 iir = addr->iir & 0xf;
//printf("ua int %x\n", addr->iir);	
	if(iir & 1)
		return;
	if(iir == 4)
		uarint(dev);
	if(iir == 2)
		uaxint(dev);
}

uaioctl(int dev, int cmd, void *arg, int mode)
{
	register struct tty *tp;

	tp = &ua_tty[dev];
	if (ttiocom(tp, cmd, arg, mode))
		if ((tp->t_cflag&CBAUD) == 0)
			; //dl_addr[dev]->rcsr &= ~(RTS|DTR);
}

uaproc(tp, cmd)
register struct tty *tp;
{
	register c;

	extern ttrstrt();

	switch (cmd) {

	case T_TIME:
		tp->t_state &= ~TIMEOUT;
		//addr->tcsr &= ~XBRK;
		goto start;

	case T_WFLUSH:
	case T_RESUME:
		tp->t_state &= ~TTSTOP;
		goto start;

	case T_OUTPUT:
	start:
		if (tp->t_state&(TIMEOUT|TTSTOP|BUSY))
			break;
		if (tp->t_state&TTIOW && tp->t_outq.c_cc==0) {
			tp->t_state &= ~TTIOW;
			wakeup((caddr_t)&tp->t_oflag);
		}
		while ((c=getc(&tp->t_outq)) >= 0) {
			if (tp->t_oflag&OPOST && c == 0200) {
				if ((c = getc(&tp->t_outq)) < 0)
					break;
				if (c > 0200) {
					tp->t_state |= TIMEOUT;
					timeout(ttrstrt, (caddr_t)tp, (c&0177));
					break;
				}
			}
			tp->t_state |= BUSY;
			addr->thr = c;
			addr->ier |= XIE;
			break;
		}
		if (tp->t_state&OASLP && tp->t_outq.c_cc<=ttlowat[tp->t_cflag&CBAUD]) {
			tp->t_state &= ~OASLP;
			wakeup((caddr_t)&tp->t_outq);
		}
		break;

	case T_SUSPEND:
		tp->t_state |= TTSTOP;
		break;

	case T_BLOCK:
		tp->t_state &= ~TTXON;
		tp->t_state |= TBLOCK;
		if (tp->t_state&BUSY)
			tp->t_state |= TTXOFF;
		else
			addr->thr = CSTOP;
		break;

	case T_RFLUSH:
		if (!(tp->t_state&TBLOCK))
			break;
	case T_UNBLOCK:
		tp->t_state &= ~(TTXOFF|TBLOCK);
		if (tp->t_state&BUSY)
			tp->t_state |= TTXON;
		else
			addr->thr = CSTART;
		break;

	case T_BREAK:
		//addr->tcsr |= XBRK;
		tp->t_state |= TIMEOUT;
		timeout(ttrstrt, tp, HZ/4);
		break;
	}
}

uaclr()
{
	register dev;
	register struct tty *tp;

	for (dev = 0; dev < ua_cnt; dev++) {
		tp = &ua_tty[dev];
		if ((tp->t_state&(ISOPEN|WOPEN)) == 0)
			continue;
		addr->ier |= RIE|XIE;
		tp->t_state &= ~BUSY;
		uaproc(tp, T_OUTPUT);
	}
}

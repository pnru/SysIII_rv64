//
// assembler assists
//

TEXT w_pmpcfg0(SB),$-8
	MOVW	R8,CSR(0x03a0)
	RET

TEXT w_pmpaddr0(SB),$-8
	MOVW	R8,CSR(0x03b0)
	RET

TEXT r_time(SB),$-8
	MOV	CSR(0x0c01),R8
	RET
	
TEXT w_pnr(SB),$-8
	MOVW	R8,CSR(0x0101)
	RET

TEXT w_trace(SB),$-8
	MOVW	R8,CSR(0x005)
	RET

TEXT r_mstatus(SB),$-8
	MOVW	CSR(0x0300),R8
	RET

TEXT w_mstatus(SB),$-8
	MOVW	R8,CSR(0x0300)
	RET

TEXT w_mepc(SB),$-8
	MOVW	R8,CSR(0x0341)
	RET

TEXT w_medeleg(SB),$-8
	MOVW	R8,CSR(0x0302)
	RET

TEXT w_mideleg(SB),$-8
	MOVW	R8,CSR(0x0303)
	RET

TEXT r_sie+0(SB),$-8
	MOVW	CSR(0x0104),R8
	RET

TEXT w_sie+0(SB),$-8
	MOVW	R8,CSR(0x0104)
	RET

TEXT r_sip+0(SB),$-8
	MOVW	CSR(0x0144),R8
	RET

TEXT r_mip+0(SB),$-8
	MOV	CSR(0x0344),R8
	RET
	
TEXT w_sip+0(SB),$-8
	MOVW	R8,CSR(0x0144)
	RET

TEXT r_mie+0(SB),$-8
	MOVW	CSR(0x0304),R8
	RET

TEXT w_mie+0(SB),$-8
	MOVW	R8,CSR(0x0304)
	RET

TEXT w_mscratch+0(SB),$-8
	MOVW	R8,CSR(0x0340)
	RET
	
TEXT w_mtvec+0(SB),$-8
	MOVW	R8,CSR(0x0305)
	RET

TEXT w_stvec(SB),$-8
	MOVW	R8,CSR(0x0105)
	RET

TEXT r_sstatus(SB),$-8
	MOVW	CSR(0x0100),R8
	RET

TEXT w_sstatus(SB),$-8
	MOVW	R8,CSR(0x0100)
	RET

TEXT r_satp(SB),$-8
	MOVW	CSR(0x0180),R8
	RET

TEXT w_satp(SB),$-8
	MOVW	R8,CSR(0x0180)
	RET

TEXT r_sepc(SB),$-8
	MOVW	CSR(0x0141),R8
	RET

TEXT w_sepc(SB),$-8
	MOVW	R8,CSR(0x0141)
	RET
	
TEXT r_scause(SB),$-8
	MOVW	CSR(0x0142),R8
	RET
	
TEXT r_stval(SB),$-8
	MOVW	CSR(0x0143),R8
	RET

TEXT r_gp(SB),$-8
	MOV	R3,R8
	RET

#define MRET		WORD $0x30200073

TEXT do_mret(SB),$-8
	MRET

#define SFENCE_VMA	WORD $0x12000073

TEXT sfence_vma(SB),$-8
	SFENCE_VMA
	RET

#define FENCE_IR	WORD $0x0aa0000f
#define FENCE_OW	WORD $0x0550000f
#define WFI		WORD $0x10500073

TEXT idle(SB),$-8
	WFI
	//MOV	$1,CSR(0x101)
	RET

TEXT read32(SB),$-8
TEXT readl(SB),$-8
	MOVW	0(R8),R8
	FENCE_IR
	RET
	
TEXT write32(SB),$-8
TEXT writel(SB),$-8
	MOV	8(FP),R12
	FENCE_OW
	MOVW	R12,0(R8)
	RET

//
// D1 specific d-cache management functions (for DMA)
//

#define DCACHE_IPA(rs1)	WORD	$(0x02a0000b | (rs1<<15))
#define DCACHE_CPA(rs1)	WORD	$(0x0290000b | (rs1<<15))
#define SYNC_IS		WORD	$(0x01b0000b)

TEXT dcache_ipa(SB),$-8
	DCACHE_IPA(8)
	RET
	
TEXT dcache_cpa(SB),$-8
	DCACHE_CPA(8)
	RET

TEXT sync_is(SB),$-8
	SYNC_IS
	RET

//
// process switching
//

TEXT setjmp(SB),0,$-8
TEXT save(SB),0,$-8
	// setjump to context
	MOV	R1,0(R8)
	MOV	R2,8(R8)
	MOV	R3,16(R8)
	MOV	R4,24(R8)
	MOV	R5,32(R8)
	MOV	R6,40(R8)
	MOV	R7,48(R8)
	MOV	$0,R8
	RET

#define SIE	(1L<<1)
#define SSTATUS 0x100

// resume(new_phys_u, context);
TEXT resume(SB),0,$-8

	// no interrupts
	MOVW	CSR(SSTATUS),R24
	AND	$~SIE,R24,R7
	MOVW	R7,CSR(SSTATUS)
	
	// fetch 2nd arg from old stack
	MOV	16(R2),R7

	// switch kptbl to new u & stack
	MOV	u_pte(SB),R1
	SRA	$2,R8
	OR	$0xc7,R8
	MOV	R8,0(R1)
	SFENCE_VMA

	// longjump(context, 1)
	MOV	0(R7),R1
	MOV	8(R7),R2
	MOV	16(R7),R3
	MOV	24(R7),R4
	MOV	32(R7),R5
	MOV	40(R7),R6
	MOV	48(R7),R7
	MOV	$1,R8
	MOVW	R24,CSR(SSTATUS)
	RET

TEXT longjmp(SB),0,$-8
	MOV	0(R8),R1
	MOV	8(R8),R2
	MOV	16(R8),R3
	MOV	24(R8),R4
	MOV	32(R8),R5
	MOV	40(R8),R6
	MOV	48(R8),R7
	MOV	$1,R8
	RET

TEXT adjust_sp(SB),0,$-8
	// swap sp from physical page to same u virtual page
	MOV	$0x3fffffd000,R8
	AND	$0xfff,R2
	OR	R8,R2
	RET

TEXT icode(SB),0,$-8
/* 0*/	MOV	$0x0c,R8
/* 2*/	MOV	$0x14,R9
/* 4*/	MOV	$11,R15		// 11 = exec
/* 6*/	ECALL
/* a*/	JMP	0(PC)

TEXT init(SB),$-8
/* c*/	WORD	$0x696e692f	// /ini
/*10*/	WORD	$0x00000074	// t\0

TEXT argv(SB),$-8
/*14*/	WORD	$0x0c
/*18*/	WORD	$0
/*1c*/	WORD	$0
/*20*/	WORD	$0

TEXT icodeend(SB),0,$-8
	RET



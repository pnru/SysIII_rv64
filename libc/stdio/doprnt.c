/*LINTLIBRARY*/
/*
 *	_doprnt: common code for printf, fprintf, sprintf
 */

#include <stdio.h>
#include <ctype.h>
#include <varargs.h>

#define SGNBIT  0x80000000
#define MAXDIGS 11
#define MAXECVT 18
#define MAXESIZ 4
#define MAXFSIG MAXECVT
#define MAXFCVT 60

#define PUT(s, n)     { register char *str = s; register int nn; \
			for (nn = 0; nn < n; nn++) \
				fputc(*str++, iop); \
		      }

#define PAD(c, n)     { register int nn; \
			for (nn = 0; nn < n; nn++) \
				fputc(c, iop); \
		      }

#define min(a, b) ((a)<(b) ? (a) : (b))

/* bit positions for flags used in doprnt */

#define LENGTH	1	/* l */
#define FPLUS	2	/* + */
#define FMINUS	4	/* - */
#define FBLANK	8	/* blank */
#define FSHARP	16	/* # */
#define PADZERO 32	/* padding zeroes requested via '0' */
#define DOTSEEN 64	/* dot appeared in format specification */
#define SUFFIX	128	/* a suffix is to appear in the output */
#define RZERO	256	/* there will be trailing zeros in output */
#define LZERO	512	/* there will be leading zeroes in output */

/*
 *	C-Library routines for floating conversion
 */

extern char *fcvt(), *ecvt(), *memchr(), *memcpy();
extern int strlen();

static int
_lowdigit(valptr)
	long *valptr;
{	/* This function computes the decimal low-order digit of the number */
	/* pointed to by valptr, and returns this digit after dividing   */
	/* *valptr by ten.  This function is called ONLY to compute the */
	/* low-order digit of a long whose high-order bit is set. */

	int lowbit = *valptr & 1;
	long value = (*valptr >> 1) & ~SGNBIT;

	*valptr = value / 5;
	return (value % 5 * 2 + lowbit + '0');
}

int
_doprnt(format, args, iop)
	register char	*format;
	va_list	args;
	register FILE	*iop;
{
        register char	*bp;    	/* Start point for value to be printed */
        register int	fcode;		/* Format code */
        register int	flags;		/* Format modifiers */

        int	count = 0;      	/* This variable counts output characters. */
	char    *p;             	/* End point for value to be printed */
	int	width, prec;    	/* Field width and precision */
	int	lzero, rzero;		/* Number of padding zeroes required on the left and right */

	char	buf[MAXECVT+1];	        /* Values are developed in this buffer */
        char	expbuf[MAXESIZ+1];	/* Buffer to create exponent */
        
        char	*prefix;		/* Pointer to sign, "0x", "0X", or empty */
	char	*suffix;		/* Exponent or empty */
	int	prelen, suflen;		/* Length of prefix and of suffix */
	int 	misclen;		/* combined length of leading zeroes, trailing zeroes, and suffix */

	long	val;	        	/* The value being converted, if integer */
	double	dval;   		/* The value being converted, if real */
	int	decpt, sign;		/* Output values from fcvt and ecvt */

	char	*xtab;	        	/* Radix translation xtab */
	int	k, lradix, mradix;	/* Work variables */

	/*
	 *	The main loop -- this loop goes through one iteration
	 *	for each string of ordinary characters or format specification.
	 */
	for (;;) {
		register int n;

		if ((fcode = *format) != '\0' && fcode != '%') {
			bp = format;
			do {
				count++;
				putc(*format, iop);
				format++;
			} while ((fcode = *format) != '\0' && fcode != '%');
		}
		if (fcode == '\0') {  /* end of format; return */
			return (count);
		}

		/*
		 *	% has been found.
		 */
		width = prelen = misclen = flags = 0;
		format++;

	reswitch:

		switch (fcode = *format++) {

		case '+':
			flags |= FPLUS;
			goto reswitch;
		case '-':
			flags |= FMINUS;
			goto reswitch;
		case ' ':
			flags |= FBLANK;
			goto reswitch;
		case '#':
			flags |= FSHARP;
			goto reswitch;

		/* Scan the field width and precision */

		case '.':
			flags |= DOTSEEN;
			prec = 0;
			goto reswitch;

		case '*':
			if (!(flags & DOTSEEN)) {
				width = va_arg(args, int);
				if (width < 0) {
					width = -width;
					flags ^= FMINUS;
				}
			} else {
				prec = va_arg(args, int);
				if (prec < 0)
					prec = 0;
			}
			goto reswitch;

		case '0':
			/* leading zero in width means pad with leading zeros */
			if (!(flags & (DOTSEEN | FMINUS)))
				flags |= PADZERO;

		case '1':	case '2':	case '3':	case '4':
		case '5':	case '6':	case '7':	case '8':
		case '9':
		      { 
			register num = fcode - '0';
			while (isdigit(fcode = *format)) {
				num = num * 10 + fcode - '0';
				format++;
			}
			if (flags & DOTSEEN)
				prec = num;
			else
				width = num;
			goto reswitch;
		      }

		/* Scan the length modifier */
		case 'l':
			flags |= LENGTH;
			/* No break */
		case 'h':
			goto reswitch;

		/*
		 *	decimal fixed point representations
		 *
		 *	We assume a 2's complement machine
		 */

		case 'd':
			/* Fetch the argument to be printed */
			if (flags & LENGTH)
				val = va_arg(args, long);
			else
				val = va_arg(args, int);

			/* Set buffer pointer to last digit */
			p = bp = buf + MAXDIGS;

			/* If signed conversion, make sign */
			if (val < 0) {
				prefix = "-";
				prelen = 1;
				/*
				 * Negate, checking in
				 * advance for possible
				 * overflow.
				 */
				if (val != SGNBIT)
					val = -val;
				else     /* number is -SGNBIT; convert last */
					 /* digit now and get positive number */
					*--bp = _lowdigit(&val);
			} else if (flags & FPLUS) {
				prefix = "+";
				prelen = 1;
			} else if (flags & FBLANK) {
				prefix = " ";
				prelen = 1;
			}

		decimal:
			{ register long qval = val;
				if (qval <= 9) {
					if (qval != 0 || !(flags & DOTSEEN))
						*--bp = qval + '0';
				} else {
					do {
						n = qval;
						qval /= 10;
						*--bp = n - qval * 10 + '0';
					} while (qval > 9);
					*--bp = qval + '0';
				}
			}

			/* Calculate minimum padding zero requirement */
			if (flags & DOTSEEN) {
				register leadzeroes = prec - (p - bp);
				if (leadzeroes > 0) {
					misclen = lzero = leadzeroes;
					flags |= LZERO;
				}
			}

			break;

		case 'u':
			/* Fetch the argument to be printed */
			if (flags & LENGTH)
				val = va_arg(args, long);
			else
				val = va_arg(args, unsigned);

			p = bp = buf + MAXDIGS;

			if (val & SGNBIT)
				*--bp = _lowdigit(&val);

			goto decimal;

		case 'o':
			mradix = 7;
			lradix = 2;
			goto fixed;

		case 'X':
		case 'x':
			mradix = 15;
			lradix = 3;

		fixed:
			/* Fetch the argument to be printed */
			if (flags & LENGTH)
				val = va_arg(args, long);
			else
				val = va_arg(args, unsigned);

			/* Set translate table for digits */
			xtab = (fcode == 'X') ?
			    "0123456789ABCDEF" : "0123456789abcdef";

			/* Develop the digits of the value */
			p = bp = buf + MAXDIGS;
			{ register long qval = val;
				if (qval == 0) {
					if (!(flags & DOTSEEN)) {
						misclen = lzero = 1;
						flags |= LZERO;
					}
				} else
					do {
						*--bp = xtab[qval & mradix];
						qval = ((qval >> 1) & ~SGNBIT) >> lradix;
					} while (qval != 0);
			}

			/* Calculate minimum padding zero requirement */
			if (flags & DOTSEEN) {
				register leadzeroes = prec - (p - bp);
				if (leadzeroes > 0) {
					misclen = lzero = leadzeroes;
					flags |= LZERO;
				}
			}

			/* Handle the # flag */
			if (flags & FSHARP && val != 0)
				switch (fcode) {
				case 'o':
					if (!(flags & LZERO)) {
						misclen = lzero = 1;
						flags |= LZERO;
					}
					break;
				case 'x':
					prefix = "0x";
					prelen = 2;
					break;
				case 'X':
					prefix = "0X";
					prelen = 2;
					break;
				}

			break;

			
		case 'G':
		case 'g':			
			/* Establish default precision */
			if (!(flags & DOTSEEN))
				prec = 6;
			else if (prec == 0)
				prec = 1;

			/* Fetch the value */
			dval = va_arg(args, double);

			/* Do the conversion */
			bp = ecvt(dval, min(prec, MAXECVT), &decpt, &sign);
			if (dval == 0)
				decpt = 1;
			{ register int kk = prec;
				if (!(flags & FSHARP)) {
					n = strlen(bp);
					if (n < kk)
						kk = n;
					while (kk >= 1 && bp[kk-1] == '0')
						--kk;
				}
				
				if (decpt < -3 || decpt > prec) {
					prec = kk - 1;
					goto e_merge;
				}
				prec = kk - decpt;
				goto f_merge;
			}
			break;

		case 'E':
		case 'e':
			/* Establish default precision */
			if (!(flags & DOTSEEN))
				prec = 6;

			/* Fetch the value */
			dval = va_arg(args, double);

			/* Develop the mantissa */
			bp = ecvt(dval, min(prec + 1, MAXECVT), &decpt, &sign);

		e_merge:
			/* Determine the prefix */
			if (sign) {
				prefix = "-";
				prelen = 1;
			} else if (flags & FPLUS) {
				prefix = "+";
				prelen = 1;
			} else if (flags & FBLANK) {
				prefix = " ";
				prelen = 1;
			}

			/* Place the first digit in the buffer*/
			p = &buf[0];
			*p++ = (*bp != '\0') ? *bp++ : '0';

			/* Put in a decimal point if needed */
			if (prec != 0 || (flags & FSHARP))
				*p++ = '.';

			/* Create the rest of the mantissa */
			{ register rz = prec;
				for ( ; rz > 0 && *bp != '\0'; --rz)
					*p++ = *bp++;
				if (rz > 0) {
					misclen = rzero = rz;
					flags |= RZERO;
				}
			}

			bp = &buf[0];

			/* Create the exponent */
			*(suffix = &expbuf[MAXESIZ]) = '\0';
			if (dval != 0) {
				register int nn = decpt - 1;
				if (nn < 0)
				    nn = -nn;
				for ( ; nn > 9; nn /= 10)
					*--suffix = (nn % 10) + '0';
				*--suffix = (nn) + '0';
			}

			/* Prepend leading zeroes to the exponent */
			while (suffix > &expbuf[MAXESIZ - 2])
				*--suffix = '0';

			/* Put in the exponent sign */
			*--suffix = (decpt > 0 || dval == 0) ? '+' : '-';

			/* Put in the e */
			*--suffix = isupper(fcode) ? 'E'  : 'e';

			/* compute size of suffix */
			misclen += (suflen = &expbuf[MAXESIZ]
								 - suffix);
			flags |= SUFFIX;
			break;

		case 'f':
			/* Establish default precision */
			if (!(flags & DOTSEEN))
				prec = 6;

			/* Fetch the value */
			dval = va_arg(args, double);

			/* Do the conversion */
			bp = fcvt(dval, min(prec, MAXFCVT), &decpt, &sign);

		f_merge:
			/* Determine the prefix */
			if (sign && decpt > -prec && *bp != '0') {
				prefix = "-";
				prelen = 1;
			} else if (flags & FPLUS) {
				prefix = "+";
				prelen = 1;
			} else if (flags & FBLANK) {
				prefix = " ";
				prelen = 1;
			}

			/* Initialize buffer pointer */
			p = &buf[0];

			{ register int nn = decpt;

				/* Emit the digits before the decimal point */
				k = 0;
				do {
					*p++ = (nn <= 0 || *bp == '\0' 
						|| k >= MAXFSIG) ?
				    		'0' : (k++, *bp++);
				} while (--nn > 0);

				/* Decide whether we need a decimal point */
				if ((flags & FSHARP) || prec > 0)
					*p++ = '.';

				/* Digits (if any) after the decimal point */
				nn = min(prec, MAXFCVT);
				if (prec > nn) {
					flags |= RZERO;
					misclen = rzero = prec - nn;
				}
				while (--nn >= 0)
					*p++ = (++decpt <= 0 || *bp == '\0' ||
				   	    k >= MAXFSIG) ? '0' : (k++, *bp++);
			}

			bp = &buf[0];
			break;

		case '%':
			buf[0] = fcode;
			goto c_merge;

		case 'c':
			buf[0] = va_arg(args, int);
		c_merge:
			p = (bp = &buf[0]) + 1;
			break;

		case 's':
			bp = va_arg(args, char *);
			if (!(flags & DOTSEEN))
				p = bp + strlen(bp);
			else { /* a strnlen function would  be useful here! */
				register char *qp = bp;
				while (*qp++ != '\0' && --prec >= 0)
					;
				p = qp - 1;
			}
			break;

		default: 
			format--;
			continue;

		}

		/* Calculate number of padding blanks */
		k = (n = p - bp) + prelen + misclen;
		if (width <= k)
			count += k;
		else {
			count += width;

			/* Set up for padding zeroes if requested */
			/* Otherwise emit padding blanks unless output is */
			/* to be left-justified.  */

			if (flags & PADZERO) {
				if (!(flags & LZERO)) {
					flags |= LZERO;
					lzero = width - k;
				}
				else
					lzero += width - k;
				k = width; /* cancel padding blanks */
			} else
				/* Blanks on left if required */
				if (!(flags & FMINUS))
					PAD(' ', width - k);
		}

		/* Prefix, if any */
		if (prelen != 0)
			PUT(prefix, prelen);

		/* Zeroes on the left */
		if (flags & LZERO)
			PAD('0', lzero);
		
		/* The value itself */
		if (n > 0)
			PUT(bp, n);

		if (flags & (RZERO | SUFFIX | FMINUS)) {
			/* Zeroes on the right */
			if (flags & RZERO)
				PAD('0', rzero);

			/* The suffix */
			if (flags & SUFFIX)
				PUT(suffix, suflen);

			/* Blanks on the right if required */
			if (flags & FMINUS && width > k)
				PAD(' ', width - k);
		}
	}
}

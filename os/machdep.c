#include "sys/types.h"
#include "sys/param.h"
#include "sys/systm.h"
#include "sys/dir.h"
#include "sys/user.h"
#include "sys/inode.h"
#include "sys/proc.h"
#include "sys/seg.h"
#include "sys/map.h"
#include "sys/psl.h"
#include "sys/utsname.h"
#include "sys/clock.h"
#include "sys/page.h"
#include "sys/vm.h"
#include "sys/var.h"
#include "sys/text.h"

/*
 * Initialise device drivers.
 */
extern int (*dev_init[])();

void devinit(void)
{
	int i;

	for (i=0; i<10 ; i++) {
		if (dev_init[i])
			(dev_init[i])();
		else
			break;
	}
}

/*
 * Machine-dependent startup code
 */
struct user *
startup(firstaddr)
{
	register int i, n;
	struct user *upa;
	extern char end[], etext[];
	int ts, ds;
	
	/* Locate kernel user page / stack */
	upa = (struct user *) PGROUNDUP((u64)end);
//printf("upa=%p\n", upa);
	
	/*
	 * Initialize maps
	 */
	printf("\nUNIX/%s: %s %s\n", utsname.release, utsname.sysname, utsname.version);
	printf("real mem  = %d\n", maxmem*ctob(1) );
	maxmem = meminit() / PGSIZE;
	printf("avail mem = %d\n", maxmem*ctob(1));
	ts = ((u64)etext - 0x40000000);
	ds = (end - etext);
	printf("end = %p, text = %dKB, data = %dKB\n", end, ts/1024, ds/1024);
	printf("pgcount = %d\n", pgcount);
//memcheck("su");
	if(MAXMEM < maxmem)
		maxmem = MAXMEM;
	mfree(swapmap, nswap, 1);
	swplo--;
	
	/* Set up kernel page table */
	kpt_init(upa);
	devinit();
	
//memcheck("s2");
	return upa;
}

clkset(oldtime)
time_t	oldtime;
{
	time = oldtime;
}

/*
 * Send an interrupt to process
 */
sendsig(p, n)
u64 p, n;
{
	register u64 *usp;

	usp = (u64 *)(u.u_pcb.R2);
	grow((usp - 2));
	suword( (caddr_t) --usp, u.u_pcb.epc);	/* save epc */
	suword( (caddr_t) --usp, u.u_pcb.R8);	/* save R8 */
	u.u_pcb.R8  = n; 	/* R8 = signo */
	u.u_pcb.R2  = (u64)usp;
	u.u_pcb.epc = p;
}

/*
 * copy count bytes from from to to.
 */
void bcopy(void* from, void* to, int n)
{
	register char *s1 = (char*)from;
	register char *s2 = (char*)to;
if (s2<(char*)0x40000000) panic("bad bcpy\n");
	while(n-- > 0){
		*s2++ = *s1++;
	}
}

void memset(void* dst, int c, int n)
{
	register char *s1 = (char*)dst;
if (s1<(char*)0x40000000) panic("bad mset\n");
	while(n-- > 0){
		*s1++ = c;
	}
}

/* Create a new user page table for a given process,
 * with no user memory, but with trampoline & u pages mapped
 * Called from exec and main.
 */
pagetable_t new_pt(struct user *old_u)
{
	pagetable_t pt;
	u64	va;
	void	*pa;
// printf("old_u=%p\n", old_u);
	/* create an empty page table */
	if((pt = (pagetable_t)kalloc()) == 0)
		return NULL;
		
	/* map the trampoline page; note no PTE_U */
	if (mappages(pt, TRAMPOLINE, PGSIZE, (u64)trampo, PTE_R|PTE_X) < 0) {
		uvmzap(pt);
		return NULL;
	}

	/* map the u page code; note no PTE_U */
	if (mappages(pt, (u64)&u,    PGSIZE, (u64)old_u,  PTE_R|PTE_W) < 0) {
		uvmunmap(pt, TRAMPOLINE, 1, 0);
		uvmzap(pt);
		return NULL;
	}

	return pt;
}

/* Free data, stack and page-tables. The u-area is not freed.
 */
void imgzap(struct proc *p)
{
	/* a fully swapped process has no img */
//printf("zap pid=%d\n", p->p_pid);
	if (p->p_pt) {
		uvmunmap(p->p_pt, (u64)&u, 1, 0);
		uvmunmap(p->p_pt, TRAMPOLINE, 1, 0);
		uvmzap(p->p_pt);
	}
	p->p_pt = NULL;
}

void audit(char *s)
{
	struct proc *p;
	struct text *xp;
	pte_t *pte;
	int i = 0;

	//printf(".");
	for (p = &proc[1]; p<(struct proc *)v.ve_proc; p++) {
		if (p->p_stat==0 || p->p_pt==NULL) continue;
		if (p->p_pt > (pagetable_t)0x80000000LL) {
			printf("%s: pid %d has bad pt\n", s, p->p_pid);
			i++;
			continue;
		}
		xp = p->p_textp;
		pte = walk(p->p_pt, (u64)0, 0);
		if ((p == xp->x_procp) && pte && (*pte & PTE_L)) {
			printf("%s: pid %d is primary but with linked text\n", s, p->p_pid);
			i++;
		}
	}
	if (i) panic("bad link\n");
}

/* Create a duplicate copy of a process's memory. Called from fork.
 */
procdup(op, np)
register struct proc *op, *np;
{
	struct user *new_u;
	struct text *xp;
	void* pt;
	unsigned sz;

	/* allocate new user struct */
	if((new_u = kalloc()) == 0)
		return(NULL);
	
	/* start a new page table */
	if((pt = kalloc()) == 0) {
		kfree(new_u);
		return(NULL);
	}
//printf("ts=%x\n", u.u_tsize);
//printf("ds=%x\n", u.u_dsize);
//printf("ss=%x\n", u.u_ssize);
	/* copy page table & pages */
//printf("copy text\n");
	sz = u.u_tsize;
	xp = op->p_textp;
	if (xp) xlock(xp);
	if(uvmcopy(op->p_pt, pt, 0, sz, 1) == -1){
		if (xp) xunlock(xp);
		uvmzap((pagetable_t)pt);
		kfree(new_u);
		return(NULL);
	}
	if (xp) xunlock(xp);

//printf("copy data\n");
	sz = u.u_dsize;
	if(uvmcopy(op->p_pt, pt, u.u_tsize, sz, 0) == -1){
		uvmzap((pagetable_t)pt);
		kfree(new_u);
		return(NULL);
	}
//printf("copy stack\n");
	sz = u.u_ssize;
//printf("dup: ss=%p\n", sz);
	if(uvmcopy(op->p_pt, pt, USRSTACK-sz, sz, 0) == -1){
		uvmzap((pagetable_t)pt);
		kfree(new_u);
		return(NULL);
	}

	/* copy the u-area, update proc and user with new mem info */
	bcopy(&u, new_u, PGSIZE);
	kvmmap(pt, (u64)&u,    (u64)new_u,  1, PTE_R|PTE_W|PTE_V);
	kvmmap(pt, TRAMPOLINE, (u64)trampo, 1, PTE_R|PTE_W|PTE_X);
	np->p_upa = new_u;
	np->p_pt  = pt;
//printf("dup: u=%p, pt=%p\n", new_u, pt);
//printf("fork %d -> %d\n", op->p_pid, np->p_pid);
//ptaudit(op, "procdup op", 0, 0);
//ptaudit(np, "procdup np", 0, 0);
	return(1);
}

int chksize(u64 text, u64 data, u64 stack)
{
	u64 sz;
	
	sz = (PGROUNDUP(text) + PGROUNDUP(data) + PGROUNDUP(stack)) / PGSIZE;
//printf("size in pages: %p, %x, %x\n", sz, MAXUMEM, maxmem);
	if (sz > MAXUMEM || sz > maxmem) {
		u.u_error = ENOMEM;
		return(-1);
	}
	return(0);
}
extern int cnt;
ptaudit(p, s, os, oss)
struct proc *p;
char *s;
{
	int ts = p->p_tsize;
	int ss = oss==-1 ? p->p_ssize : oss;
	int ds = (os || oss==0) ? os - ss : p->p_size - ss;
	int sw = p->p_swsize;
	int i, n = 0;
	u64 addr;
	
	//printf("ptaudit\n");
	if (p->p_pt==0)
		goto err;
		
	n++;

	/* text */
	for(i = 0; i<ts; i++) {
		addr = ctob(i);
		if (walkaddr(p->p_pt, addr)==0)
			goto err;
		}
	n++;
	/* data */
	if (ds > sw)		
		for(i = ts+sw; i<ts+ds; i++) {
			addr = ctob(i);
			if (walkaddr(p->p_pt, addr)==0)
				goto err;
		}
	n++;
	/* stack */
	sw = (sw > ds) ? sw -= ds : 0;
	if (ss > sw)
		for(i = sw; i<ss; i++) {
			addr = 0x80000000 - ctob(ss-i);
			if (walkaddr(p->p_pt, addr)==0)
				goto err;
		}
	return;

err:
	//if (n==0 || i==3) return;
	printf("\npt audit error %d '%s': pid=%d cnt=%d\n", n, s, p->p_pid, cnt);
	printf("%s: ts=%d ds=%d ss=%d sw=%d\n", s, ts, ds, ss, sw);
	printf("%s: missing %d\n", s, i);
	uvmlist(p->p_pt, addr - ctob(6), 12);
	panic("stop");
}

imgclr(lo, hi)
u64 lo, hi;
{
	u64 va;
	void *pa;

	for(va=lo; va<hi; va+=PGSIZE) {
		pa = (void*)walkaddr(u.u_procp->p_pt, va);
		if (pa==0) panic("imgclr");
		memset(pa, 0, PGSIZE);
	}
}

/* Keep this struct aligned with the corresponding struct in user.h
 */
void
addupc(pc, p)
	word pc;
	struct {			/* profile arguments */
		word	*pr_base;	/* buffer base */
		unsigned pr_size;	/* buffer size */
		unsigned pr_off;	/* pc offset */
		unsigned pr_scale;	/* pc scaling */
	} *p;
{
	if (p->pr_scale == 0)
		return;
	if (pc < p->pr_off)
		return;
	pc -= p->pr_off;
	pc /= ~(p->pr_scale) + 1;
	pc  = (pc+2) / 4;
	if (pc > p->pr_size)
		return;
	p->pr_base[pc] += 1;
	return;
}

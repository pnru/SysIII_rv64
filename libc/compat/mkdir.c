#include <errno.h>

mkdir(path, mode)
char *path;
int mode;
{
	register int child, pid;
	int status;
	extern errno;
	int mask;

	switch (child = fork()) {
	case -1:
		return(-1);
	case 0:
		/* so we don't get any output on errors */
		close(1);
		close(2);

		execl("/bin/mkdir", "mkdir", path, 0);
		exit(2);
	default:
		while ((pid = wait(&status)) > 0)
			if (pid == child)
				break;
		if (pid < 0 || status == (2<<8)) {
			errno = ENOEXEC;
			return(-1);
		}
		if (status) {
			errno = EACCES;
			return(-1);
		}
		/*
		 * Find out the current umask, modify the mode
		 * accordingly, and set the mode on the newly
		 * created directory.  Note that we can't get
		 * the umask value without changing it, so we
		 * have to do a second umask to restore it.
		 */
		mask = umask(0);
		umask(mask);
		mode &= ~mask;
		if (chmod(path, mode) < 0)
			return(-1);
		return(0);
	}
}


/*
 *	Memory special file
 *	minor device 0 is physical memory
 *	minor device 1 is kernel memory
 *	minor device 2 is EOF/NULL
 */

#include "sys/param.h"
#include "sys/types.h"
#include "sys/dir.h"
#include "sys/var.h"
#include "sys/user.h"
#include "sys/buf.h"
#include "sys/systm.h"
#include "sys/vm.h"

mmread(dev)
{
	register unsigned n;
	register c;

	while(u.u_error==0 && u.u_count!=0) {
		n = min(u.u_count, BSIZE);
		
		switch(dev) {
		case 0: /* mem */
		case 1: /* kmem */
			if (copyout((caddr_t)u.u_offset, (u64)u.u_base, n))
				u.u_error = ENXIO;
		case 2: /* null */
			break;
		case 3: /* var */
			if ( u.u_offset < 0 || u.u_offset+n > sizeof(struct var)) 
				u.u_error = ENXIO;
			else if (copyout(((caddr_t)&v)+u.u_offset, (u64)u.u_base, n))
				u.u_error = ENXIO;
		}
		u.u_offset += n;
		u.u_base += n;
		u.u_count -= n;
	}
}

mmwrite(dev)
{
	register unsigned n;
	register c;

	while(u.u_error==0 && u.u_count!=0) {
		n = min(u.u_count, BSIZE);

		switch(dev) {
		case 0: /* mem */
		case 1: /* kmem */
			if (copyin((u64)u.u_base, (caddr_t)u.u_offset, n))
				u.u_error = ENXIO;
		case 2: /* null */
		case 3: /* proc */
			break;
		}
		u.u_offset += n;
		u.u_base += n;
		u.u_count -= n;
	}
}

#include "sys/types.h"
#include "sys/param.h"
#include "sys/systm.h"
#include "sys/utsname.h"

/* define 4096 bytes of aligned stack for entry.s */
i64 stack0[512];

void main();
void timerinit();

/* set up base/bounds memory control */

void w_pmpcfg0(u64 x);
void w_pmpaddr0(u64 x);

void pmpinit(void)
{
	// see https://l1nxy.me/2021/05/26/fix-xv6-boot/
	w_pmpaddr0(0xffffffffull);
	w_pmpcfg0(0x0full); // PMP_R | PMP_W | PMP_X | PMP_MATCH_TOR);
}

/* location of platform level interupt controller */
#define PLIC 		0x10000000L
#define PLIC_CTRL      (PLIC + 0x1ffffc)

/* Machine control assists */
#define MSTATUS_MPP_MASK	(3L << 11) // previous mode.
#define MSTATUS_MPP_S		(1L << 11)

// Machine-mode Interrupt Enable
#define MIE_MEIE (1L << 11) // external
#define MIE_MTIE (1L << 7)  // timer
#define MIE_MSIE (1L << 3)  // software

#define SIE_SEIE (1L << 9) // external
#define SIE_STIE (1L << 5) // timer
#define SIE_SSIE (1L << 1) // software

//#define SSTATUS_SIE (1L << 1)
#define MSTATUS_MIE (1L << 3)

u64 r_mstatus(void);
u64 r_sstatus(void);
u64 r_time(void);
u64 r_mie(void);
u64 r_sie(void);
void w_mstatus(u64 x);
void w_sstatus(u64 x);
void w_medeleg(u64 x);
void w_mideleg(u64 x);
void w_satp(u64 x);
void w_mepc(u64 x);
void w_mie(u64 x);
void w_sie(u64 x);
void do_mret(void);

void printf(...);

#define D1_CCU_BASE             (0x02001000) 
#define CCM_HCLKGATE0_BASE 	((void*)(D1_CCU_BASE + 0x84c))
#define CCM_HCLKRST0_BASE 	((void*)(D1_CCU_BASE + 0x84c))
#define CCMU_MMC0_CLK_BASE 	((void*)(D1_CCU_BASE + 0x830))

void* smhc_clk_init(void)
{
	u32 rval;

	// Open clock gate SMHC0
	rval = readl(CCM_HCLKGATE0_BASE);
	rval |= 0x01; 
	writel(CCM_HCLKGATE0_BASE, rval);

	// Clear reset SHMC0
	rval = readl(CCM_HCLKRST0_BASE);
	rval |= (1 << 16); 
	writel(CCM_HCLKRST0_BASE, rval);

	/* Config mod clock */
	writel(CCMU_MMC0_CLK_BASE, 0x80000000);
	return CCMU_MMC0_CLK_BASE;
}


/* entry.s jumps here. CPU is in machine mode */

void start(u32 firstaddr)
{
	printf("Unix booting...\n");
	
	/* set M Previous Privilege mode to Supervisor, for mret */
	u64 x = r_mstatus();
	x &= ~MSTATUS_MPP_MASK;
	x |=  MSTATUS_MPP_S | (1 << 13);
	w_mstatus(x);

	/* set M Exception Program Counter to main, for mret */
	w_mepc((u64)main);

	/* disable paging for now */
	w_satp(0);

	/* delegate all interrupts and exceptions to supervisor mode */
	w_medeleg(0xffff);
	w_mideleg(0xffff);
	w_sstatus(r_sstatus() & ~SSTATUS_SIE);
	w_sie(r_sie() | SIE_SEIE | SIE_STIE | SIE_SSIE);

	/* set up clock interrupts */
	pmpinit();
	timerinit();
	/* activate SMHC clock -- temp fix */
	smhc_clk_init();

	*(u32*)PLIC_CTRL = 1;

	/* jump to main(), switch to supervisor mode */
	do_mret();
}


#define CLINT 0x14000000L
#define CLINT_MTIMECMP(hartid) (CLINT + 0x4000 + 8*(hartid))

// a scratch area per CPU for machine-mode timer interrupts.
u64 timer_scratch[5];
u64 hw_timer;

// assembly code in kernelvec.s for machine-mode timer interrupt.
extern void timervec();
// set up to receive timer interrupts in machine mode,
// which arrive at timervec in kernelvec.S,
// which turns them into software interrupts for
// devintr() in trap.c.
void
timerinit()
{
	// ask the CLINT for a timer interrupt.
	int interval = 10000; // cycles; about 1/10th second in qemu.

	u64 t = r_time() + interval;
	*(u32*)(CLINT_MTIMECMP(0)  ) = t & 0xffffffff;
	*(u32*)(CLINT_MTIMECMP(0)+4) = t >> 32;

	// prepare information in scratch[] for timervec.
	// scratch[0..2] : space for timervec to save registers.
	// scratch[3] : address of CLINT MTIMECMP register.
	// scratch[4] : desired interval (in cycles) between timer interrupts.
	u64 *scratch = &timer_scratch[0];
	scratch[3] = CLINT_MTIMECMP(0);
	scratch[4] = (u64)(&hw_timer);
	w_mscratch((u64)scratch);

	// set the machine-mode trap handler.
	w_mtvec((u64)timervec);

	// enable machine-mode interrupts.
	w_mstatus(r_mstatus() | MSTATUS_MIE);
	// enable machine-mode timer interrupts.
	w_mie(r_mie() | MIE_MEIE | MIE_MTIE | MIE_MSIE);
}



int
mkstemp (template)
	char *template;
{
	static int value;
	char *XXXXXX;
	int len, count, now[2];

	len = strlen (template);
	if (len < 6 || strncmp (&template[len - 6], "XXXXXX", 6)) {
		return -1;
	}
	XXXXXX = &template[len - 6];
	time (now);
	value += getpid () + now[0] + now[1];

	for (count = 0; count < 100; ++count) {
		register int v = value & 077777;
		int fd, fd2;

		/* Fill in the random bits.  */
		XXXXXX[0] = (v & 31) + 'A';
		v >>= 5;
		XXXXXX[1] = (v & 31) + 'A';
		v >>= 5;
		XXXXXX[2] = (v & 31) + 'A';
		fd = creat (template, 0600);
		if (fd >= 0) {
			/* The file did not exist, reopen for rd/wr  */
			fd2 = open (template, 2);
			close(fd);
			return fd2;
		}

		value += 7777;
	}

	/* We return the null string if we can't find a unique file name.  */
	template[0] = '\0';
	return -1;
}


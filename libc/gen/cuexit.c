
/* C library -- exit	*/
/* exit(code) */

#define	NEXITS	32

void (*__functab[NEXITS])(void);
int __funccnt = 0;

extern void _exit();

static void
_calls(void)
{
	register int i = __funccnt;
	
	/* "Called in reversed order of their registration" */
	while (--i >= 0)
		(*__functab[i])();
}

exit(status)
int status;
{
	_calls();
	_cleanup();
	_exit(status);
}

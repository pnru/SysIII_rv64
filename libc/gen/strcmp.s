	data	1
	text
	global	strcmp
strcmp:
	link	%fp,&F%1
	movm.l	&M%1,S%1(%fp)
	mov.l	8(%fp),%a2
	mov.l	12(%fp),%a3
L%13:
	mov.b	(%a2),%d0
	cmp.b	%d0,(%a3)+
	bne	L%14
	tst.b	(%a2)+
	bne	L%15
	mov.l	&0,%d0
	br	L%12
L%15:
	br	L%13
L%14:
	mov.b	(%a2),%d0
	ext.w	%d0
	ext.l	%d0
	mov.b	-(%a3),%d1
	ext.w	%d1
	ext.l	%d1
	sub.l	%d1,%d0
	br	L%12
L%12:
	movm.l	S%1(%fp),&M%1
	unlk	%fp
	rts
	set	S%1,-8
	set	T%1,-8
	set	F%1,-12
	set	M%1,06000
	data	1

/*
 * Ram disk driver
 */

#include "sys/types.h"
#include "sys/param.h"
#include "sys/dir.h"
#include "sys/user.h"
#include "sys/buf.h"
#include "sys/elog.h"
#include "sys/iobuf.h"
#include "sys/systm.h"
#include "sys/vm.h"
#include "sd.h"

struct	iobuf	sdtab;

struct smhc *host;
struct sdcard *sd;

sdinit()
{
	
	host = smhc0_init();
	if (!host) {
		panic("could not init SD card controller");
	}
	else {
		sd = card_mount(host);
		if (!sd)
			printf("could not init SD card\n");
	}
	if (sd->read_bl_len > BSIZE)
		panic("SD card sector size > buffer size!");
}

sdopen(int dev)
{
}

sdclose(int dev)
{
}

sdstrategy(struct buf *bp)
{
	if ( (bp->b_blkno > 20000) || (bp->b_blkno < 0) ) 
	{
printf("SD disk blkno = %d\n", bp->b_blkno);
		bp->b_flags |= B_ERROR;
		iodone(bp);
		return;
	}

	bp->av_forw = 0;
	/* link buffer to end of service queue */
	spl7();
	if (sdtab.b_actf == 0)
		sdtab.b_actf = bp;
	else
		sdtab.b_actl->av_forw = bp;
	sdtab.b_actl = bp;
	/* sdtab.b_active is always 0 for now */
	if (sdtab.b_active == 0)
		sdstart();
	spl0();
}

int card_io(void *dst, unsigned long blk, int write)
{
	struct sd_cmd	cmd;
	struct sd_data	data;

	cmd.cmdidx = write ? CMD_WRITE_SINGLE_BLOCK
			   : CMD_READ_SINGLE_BLOCK;

	if (sd->high_capacity)
		cmd.cmdarg = blk;
	else
		cmd.cmdarg = blk * sd->read_bl_len;

	cmd.resp_type = write ? MMC_RSP_R1b : MMC_RSP_R1;
	cmd.flags     = 0;

	data.b.dest    = dst;
	data.blocks    = 1;
	data.blocksize = sd->read_bl_len;
	data.flags     = write ? MMC_DATA_WRITE : MMC_DATA_READ;

	if (host_send_cmd(sd, &cmd, &data)) {
		printf("[card] block io failed\n");
		return 0;
	}
	return 1;
}

static int ioloop(int block, char* to, int len, int wr)
{
//printf("SD DSK %s %d, %d, %p\n", wr ? "WR" : "RD", block, len, to);

	if (!sd)
		return 1;
	
	while(len>0) {
		if (!card_io(to, block, wr)) {
			return 1;
		}
		len -= sd->read_bl_len;
		spl0();
		spl7();
	}
	return 0;
}

sdstart(void)
{
	register struct buf *bp;
	int rc, wr;

	/* Unlink immediately; there is always 0 or 1 request in the queue */
	if ((bp = sdtab.b_actf) == 0)
		return;
	sdtab.b_actf = bp->av_forw;
	
	/* Read or write the buffer to the CF Card. Normal I/O to a kernel
	 * disk buffer is easy, direct I/O to user space (swapping, "raw" disk
	 * access) requires juggling the page map.
	 */
	if(bp->b_flags & B_PHYS) {
		printf("direct I/O not yet implemented\n");
	} else {
		wr = !(bp->b_flags & B_READ);
		rc = ioloop(bp->b_blkno+10000, (char*)paddr(bp),  bp->b_bcount, wr);
	}
	if(rc) 
		bp->b_flags |= B_ERROR;
	iodone(bp);
}

sdintr(void)
{
}

struct	buf	sdbuf;

sdread(int dev)
{
		physio(sdstrategy, &sdbuf, dev, B_READ);
}

sdwrite(int dev)
{
		physio(sdstrategy, &sdbuf, dev, B_WRITE);
}

